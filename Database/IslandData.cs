using System.Collections.Generic;

[System.Serializable]
public struct IslandData {
    public List<CityData>[] areas; 
}

public class IslandsData {
    public static IslandData Jawa = new IslandData {
        areas = new List<CityData>[] {
            AreaData.Jawa.banten,       // 0
            AreaData.Jawa.jakartaRaya,  // 1
            AreaData.Jawa.bandungRaya,  // 2
            AreaData.Jawa.cirebonRaya,  // 3
            AreaData.Jawa.bogorRaya,    // 4
            AreaData.Jawa.purwakarta,   // 5
            AreaData.Jawa.tasikmalaya,  // 6
            AreaData.Jawa.pekalongan,   // 7
            AreaData.Jawa.semarangRaya, // 8
            AreaData.Jawa.muriaRaya,    // 9
            AreaData.Jawa.banyumas,     // 10
            AreaData.Jawa.kedu,         // 11
            AreaData.Jawa.DIYogyakarta, // 12
            AreaData.Jawa.soloRaya,     // 13
            AreaData.Jawa.surabaya,     // 14
            AreaData.Jawa.madura,       // 15
            AreaData.Jawa.pasuruanMalang,// 16
            AreaData.Jawa.besuki,       // 17
            AreaData.Jawa.bojonegoro,   // 18
            AreaData.Jawa.gresikSidoarjo,// 19
            AreaData.Jawa.madiun,       // 20
            AreaData.Jawa.kediri        // 21
        }
    };

    public static IslandData Sumatra = new IslandData {
        areas = new List<CityData>[] {
            AreaData.Sumatra.sumatraBarat,      // 0
            AreaData.Sumatra.sumatraUtaraBarat, // 1
            AreaData.Sumatra.bengkulu,          // 2
            AreaData.Sumatra.lampung,           // 3
            AreaData.Sumatra.sumatraSelatan,    // 4
            AreaData.Sumatra.jambi,             // 5
            AreaData.Sumatra.sumatraUtaraTimur, // 6
            AreaData.Sumatra.aceh,              // 7
            AreaData.Sumatra.riau,              // 8
            AreaData.Sumatra.bangkaBelitung,    // 9
            AreaData.Sumatra.kepulauanRiau,     // 10
        }
    };

    public static IslandData BaliNusaTenggara = new IslandData {
        areas = new List<CityData>[] {
            AreaData.BaliNusaTenggara.NttPulauTimor,    // 0
            AreaData.BaliNusaTenggara.bali,             // 1
            AreaData.BaliNusaTenggara.NtbPulauLombok,   // 2
            AreaData.BaliNusaTenggara.NtbPulauSumbawa,  // 3
            AreaData.BaliNusaTenggara.NttPulauFlores,   // 4
            AreaData.BaliNusaTenggara.NttPulauSumba,    // 5
        }
    };

    public static IslandData Kalimantan = new IslandData {
        areas = new List<CityData>[] {
            AreaData.Kalimantan.kalimantanSelatan,  // 0
            AreaData.Kalimantan.kalimantanBarat,    // 1
            AreaData.Kalimantan.kalimantanTengah,   // 2
            AreaData.Kalimantan.kalimantanTimur,    // 3
            AreaData.Kalimantan.kalimantanUtara,    // 4
        }
    };

    public static IslandData GorontaloSulawesi = new IslandData {
        areas = new List<CityData>[] {
            AreaData.GorontaloSulawesi.sulawesiUtara,           // 0
            AreaData.GorontaloSulawesi.sulawesiBarat,           // 1
            AreaData.GorontaloSulawesi.sulawesiSelatanSelatan,  // 2
            AreaData.GorontaloSulawesi.sulawesiUtaraKepulauan,  // 3
            AreaData.GorontaloSulawesi.gorontalo,               // 4
            AreaData.GorontaloSulawesi.sulawesiTengah,          // 5
            AreaData.GorontaloSulawesi.sulawesiSelatanUtara,    // 6
            AreaData.GorontaloSulawesi.sulawesiTenggara,        // 7
            AreaData.GorontaloSulawesi.sulawesiSelatanTengah,   // 8
        }
    };

    public static IslandData MalukuPapua = new IslandData {
        areas = new List<CityData>[] {
            AreaData.MalukuPapua.maluku,        // 0
            AreaData.MalukuPapua.malukuUtara,   // 1
            AreaData.MalukuPapua.papuaSelatan,  // 2
            AreaData.MalukuPapua.papuaBarat,    // 3
        }
    };
}