using System.Collections.Generic;

public class DatabaseUtility {
    public static string GeneratePlateNumber(List<CityData> areaList, out CityData selectedCity, bool isPrintDetail)
    {
        Random rnd = new Random();
        
        int selector = rnd.Next(0, areaList.Count);
        CityData city = areaList[selector];
        selectedCity = city;

        // Build rear letter
        string rearLetter = "";

        if (city.isInvertedRearLetters) {
            // Format: *X
            for (int i = 0; i < rnd.Next(1, 2); i++) {
                rearLetter += GetRandomLetter();
            }
            int pos = rnd.Next(0, city.rearLetters.Length);
            rearLetter += city.rearLetters[pos];
        } else {
            // Format: X*
            int pos = rnd.Next(0, city.rearLetters.Length);
            rearLetter += city.rearLetters[pos];
            for (int i = 0; i < rnd.Next(1, 2); i++) {
                rearLetter += GetRandomLetter();
            }
        }

        int midNumber = rnd.Next(1000, 9999);

        return string.Format(
            isPrintDetail ?
            $"{city.areaName}/{city.cityName} : {city.areaCode} {midNumber} {rearLetter}" :
            $"{city.areaCode} {midNumber} {rearLetter}"
        );
    }

    public static string GeneratePlateNumber(CityData city, bool isPrintDetail)
    {
        Random rnd = new Random();
        
        // Build rear letter
        string rearLetter = "";

        if (city.isInvertedRearLetters) {
            // Format: *X
            for (int i = 0; i < rnd.Next(1, 3); i++) {
                rearLetter += GetRandomLetter();
            }
            int pos = rnd.Next(0, city.rearLetters.Length);
            rearLetter += city.rearLetters[pos];
        } else {
            // Format: X*
            int pos = rnd.Next(0, city.rearLetters.Length);
            rearLetter += city.rearLetters[pos];
            for (int i = 0; i < rnd.Next(1, 3); i++) {
                rearLetter += GetRandomLetter();
            }
        }

        int midNumber = rnd.Next(1000, 9999);

        return string.Format(
            isPrintDetail ?
            $"{city.areaName}/{city.cityName} : {city.areaCode} {midNumber} {rearLetter}" :
            $"{city.areaCode} {midNumber} {rearLetter}"
        );
    }

	public static char GetRandomLetter()
    {
        Random rnd = new Random();
        
		string companyingLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		int selector = rnd.Next(0, companyingLetters.Length);
		return companyingLetters[selector];
	}
}

namespace AreaData {
    public class Jawa {
        public static List<CityData> banten = new List<CityData> {
            CitiesData.kotaSerang,
            CitiesData.kabupatenSerang,
            CitiesData.kabupatenPandeglang,
            CitiesData.kabupatenLebak,
            CitiesData.kotaCilegon,
            CitiesData.kabupatenTangerangBanten,
        };

        public static List<CityData> jakartaRaya = new List<CityData> {
            CitiesData.kaJakartaPusat,
            CitiesData.kaJakartaBarat,
            CitiesData.kotaTangerang,
            CitiesData.kaJakartaSelatan,
            CitiesData.kotaDepok,
            CitiesData.kabupatenBekasi,
            CitiesData.kabupatenTangerangJakarta,
            CitiesData.kotaBekasi,
            CitiesData.kotaTangerangSelatan,
            CitiesData.kaJakartaUtara,
            CitiesData.kaJakartaTimur,
        };

        public static List<CityData> bandungRaya = new List<CityData> {
            CitiesData.kotaBandung,
            CitiesData.kotaCimahi,
            CitiesData.kabupatenBandungBarat,
            CitiesData.kabupatenBandung,
        };

        public static List<CityData> cirebonRaya = new List<CityData> {
            CitiesData.kotaCirebon,
            CitiesData.kabupatenCirebon,
            CitiesData.kabupatenIndramayu,
            CitiesData.kabupatenMajalengka,
            CitiesData.kabupatenKuningan,
        };

        public static List<CityData> bogorRaya = new List<CityData> {
            CitiesData.kotaBogor,
            CitiesData.kabupatenBogor,
            CitiesData.kotaSukabumi,
            CitiesData.kabupatenSukabumi,
            CitiesData.kabupatenCianjur,
        };

        public static List<CityData> purwakarta = new List<CityData> {
            CitiesData.kabupatenPurwakarta,
            CitiesData.kabupatenKarawang,
            CitiesData.kabupatenSubang,
        };

        public static List<CityData> tasikmalaya = new List<CityData> {
            CitiesData.kabupatenSumedang,
            CitiesData.kabupatenGarut,
            CitiesData.kotaTasikmalaya,
            CitiesData.kabupatenTasikmalaya,
            CitiesData.kabupatenCiamis,
            CitiesData.kabupatenPangandaran,
            CitiesData.kotaBanjar,
        };

        public static List<CityData> pekalongan = new List<CityData> {
            CitiesData.kotaPekalongan,
            CitiesData.kabupatenPekalongan,
            CitiesData.kabupatenBatang,
            CitiesData.kabupatenPemalang,
            CitiesData.kotaTegal,
            CitiesData.kabupatenTegal,
            CitiesData.kabupatenBrebes,
        };

        public static List<CityData> semarangRaya = new List<CityData> {
            CitiesData.kotaSemarang,
            CitiesData.kotaSalatiga,
            CitiesData.kabupatenSemarang,
            CitiesData.kabupatenKendal,
            CitiesData.kabupatenDemak,
        };

        public static List<CityData> muriaRaya = new List<CityData> {
            CitiesData.kabupatenPati,
            CitiesData.kabupatenKudus,
            CitiesData.kabupatenJepara,
            CitiesData.kabupatenRembang,
            CitiesData.kabupatenBlora,
            CitiesData.kabupatenGrobogan,
        };

        public static List<CityData> banyumas = new List<CityData> {
            CitiesData.kabupatenBanyumas,
            CitiesData.kabupatenCilacap,
            CitiesData.kabupatenPurbalingga,
            CitiesData.kabupatenBanjarnegara,            
        };

        public static List<CityData> kedu = new List<CityData> {
            CitiesData.kotaMagelang,
            CitiesData.kabupatenMagelang,
            CitiesData.kabupatenPurworejo,
            CitiesData.kabupatenKebumen,
            CitiesData.kabupatenTemanggung,
            CitiesData.kabupatenWonosobo,
        };

        public static List<CityData> DIYogyakarta = new List<CityData> {
            CitiesData.kotaYogyakarta,
            CitiesData.kabupatenBantul,
            CitiesData.kabupatenKulonProgo,
            CitiesData.kabupatenGunungkidul,
            CitiesData.kabupatenSleman,
        };

        public static List<CityData> soloRaya = new List<CityData> {
            CitiesData.kotaSurakarta,
            CitiesData.kabupatenSukoharjo,
            CitiesData.kabupatenKlaten,
            CitiesData.kabupatenBoyolali,
            CitiesData.kabupatenSragen,
            CitiesData.kabupatenKaranganyar,
            CitiesData.kabupatenWonogiri,
        };

        public static List<CityData> surabaya = new List<CityData> {
            CitiesData.surabaya,
        };

        public static List<CityData> madura = new List<CityData> {
            CitiesData.kabupatenPamekasan,
            CitiesData.kabupatenBangkalan,
            CitiesData.kabupatenSampang,
            CitiesData.kabupatenSumenep,
        };

        public static List<CityData> pasuruanMalang = new List<CityData> {
            CitiesData.kotaMalang,
            CitiesData.kabupatenMalang,
            CitiesData.kotaBatu,
            CitiesData.kabupatenProbolinggo,
            CitiesData.kabupatenPasuruan,
            CitiesData.kotaProbolinggo,
            CitiesData.kabupatenLumajang,
            CitiesData.kotaPasuruan,
        };

        public static List<CityData> besuki = new List<CityData> {
            CitiesData.kabupatenBondowoso,
            CitiesData.kabupatenSitubondo,
            CitiesData.kabupatenJember,
            CitiesData.kabupatenBanyuwangi,
        };

        public static List<CityData> bojonegoro = new List<CityData> {
            CitiesData.kabupatenBojonegoro,
            CitiesData.kabupatenTuban,
            CitiesData.kabupatenLamongan,
            CitiesData.kabupatenMojokerto,
            CitiesData.kotaMojokerto,
            CitiesData.kabupatenJombang,
        };

        public static List<CityData> gresikSidoarjo = new List<CityData> {
            CitiesData.kabupatenGresik,
            CitiesData.kabupatenSidoarjo,
        };

        public static List<CityData> madiun = new List<CityData> {
            CitiesData.kotaMadiun,
            CitiesData.kabupatenMadiun,
            CitiesData.kabupatenNgawi,
            CitiesData.kabupatenMagetan,
            CitiesData.kabupatenPacitan,
        };

        public static List<CityData> kediri = new List<CityData> {
            CitiesData.kotaKediri,
            CitiesData.kabupatenKediri,
            CitiesData.kabupatenBlitar,
            CitiesData.kotaBlitar,
            CitiesData.kabupatenTulungagung,
            CitiesData.kabupatenNganjuk,
            CitiesData.kabupatenTrenggalek,
        };
    }

    public class Sumatra {
        public static List<CityData> sumatraBarat = new List<CityData> {
            CitiesData.kotaPadang,
            CitiesData.kabupatenLimaPuluhKota,
            CitiesData.kabupatenPasaman,
            CitiesData.kabupatenTanahDatar,
            CitiesData.kabupatenPadangPariaman,
            CitiesData.kabupatenPesisirSelatan,
            CitiesData.kabupatenSolok,
            CitiesData.kotaSawahlunto,
            CitiesData.kabupatenSijunjung,
            CitiesData.kotaBukittinggi,
            CitiesData.kotaPayakumbuh,
            CitiesData.kotaPadangPanjang,
            CitiesData.kotaSolok,
            CitiesData.kabupatenPasamanBarat,
            CitiesData.kabupatenAgam,
            CitiesData.kabupatenKepulauanMentawai,
            CitiesData.kabupatenDharmasraya,
            CitiesData.kotaPariaman,
            CitiesData.kabupatenSolokSelatan,
        };
        public static List<CityData> sumatraUtaraBarat = new List<CityData> {
            CitiesData.kotaSibolga,
            CitiesData.kabupatenTapanuliUtara,
            CitiesData.kabupatenSamosir,
            CitiesData.kabupatenHumbangHandusutan,
            CitiesData.kabupatenToba,
            CitiesData.kotaPadangSidempuan,
            CitiesData.kabupatenTapanuliSelatan,
            CitiesData.kabupatenPadangLawasUtara,
            CitiesData.kabupatenPadangLawas,
            CitiesData.kabupatenTapanuliTengah,
            CitiesData.kabupatenNiasUtara,
            CitiesData.kabupatenMandailingNatal,
            CitiesData.kotaGunungsitoli,
            CitiesData.kabupatenNiasBarat,
            CitiesData.kabupatenNias,
            CitiesData.kabupatenNiasSelatan,
            CitiesData.kabupatenDairi,
            CitiesData.kabupatenPakpakBharat,
        };
        public static List<CityData> bengkulu = new List<CityData> {
            CitiesData.kotaBengkulu,
            CitiesData.kabupatenBengkuluSelatan,
            CitiesData.kabupatenBengkuluUtara,
            CitiesData.kabupatenRejangLebong,
            CitiesData.kabupatenKepahiang,
            CitiesData.kabupatenLebong,
            CitiesData.kabupatenMukoMuko,
            CitiesData.kabupatenSeluma,
            CitiesData.kabupatenKaur,
            CitiesData.kabupatenBengkuluTengah
        };
        public static List<CityData> lampung = new List<CityData> {
            CitiesData.kotaBandarLampung,
            CitiesData.kabupatenLampungSelatan,
            CitiesData.kotaMetro,
            CitiesData.kabupatenLampungTengah,
            CitiesData.kabupatenLampungUtara,
            CitiesData.kabupatenMesuji,
            CitiesData.kabupatenLampungBarat,
            CitiesData.kabupatenLampungTimur,
            CitiesData.kabupatenTulangBawangBarat,
            CitiesData.kabupatenPesawaran,
            CitiesData.kabupatenTulangBawang,
            CitiesData.kabupatenPringsewu,
            CitiesData.kabupatenTanggamus,
            CitiesData.kabupatenWayKanan,
            CitiesData.kabupatenPesisirSelatan,
        };

        public static List<CityData> sumatraSelatan = new List<CityData> {
            CitiesData.kotaPalembang,
            CitiesData.kabupatenMusiBanyuasin,
            CitiesData.kotaPrabumulih,
            CitiesData.kabupatenMuaraEnim,
            CitiesData.kabupatenLahat,
            CitiesData.kabupatenOganKomeringUlu,
            CitiesData.kabupatenMusiRawas,
            CitiesData.kotaLubukLinggau,
            CitiesData.kabupatenBanyuasin,
            CitiesData.kabupatenOganKomeringIlir,
            CitiesData.kabupatenPenukalAbabLematangIlir,
            CitiesData.kabupatenMusiRawasUtara,
            CitiesData.kabupatenEmpatLawang,
            CitiesData.kabupatenOganIlir,
            CitiesData.kabupatenOganKomeringUluSelatan,
            CitiesData.kotaPagaralam,
            CitiesData.kabupatenOganKomeringUluTimur,
        };

        public static List<CityData> jambi = new List<CityData> {
            CitiesData.kotaJambi,
            CitiesData.kabupatenBatanghari,
            CitiesData.kabupatenTebo,
            CitiesData.kabupatenKerinci,
            CitiesData.kabupatenTanjungJabungBarat,
            CitiesData.kabupatenMerangin,
            CitiesData.kabupatenMuaroJambi,
            CitiesData.kabupatenTanjungJabungTimur,
            CitiesData.kabupatenBungo,
            CitiesData.kabupatenSarolangun,
            CitiesData.kotaSungaiPenuh
        };

        public static List<CityData> sumatraUtaraTimur = new List<CityData> {
            CitiesData.kotaMedan,
            CitiesData.kabupatenLabuhanbatuUtara,
            CitiesData.kabupatenDeliSerdang,
            CitiesData.kotaTebingTinggi,
            CitiesData.kabupatenBatubara,
            CitiesData.kabupatenLangkat,
            CitiesData.kotaTanjungBalai,
            CitiesData.kotaBinjai,
            CitiesData.kabupatenKaro,
            CitiesData.kabupatenSimalungun,
            CitiesData.kabupatenAsahan,
            CitiesData.kotaPematangSiantar,
            CitiesData.kabupatenSerdangBegadai,
            CitiesData.kabupatenLabuhanbatu,
            CitiesData.kabupatenLabuhanbatuSelatan,
        };

        public static List<CityData> aceh = new List<CityData> {
            CitiesData.kotaBandaAceh,
            CitiesData.kabupatenAcehBesar,
            CitiesData.kabupatenAcehBaratDaya,
            CitiesData.kabupatenAcehTimur,
            CitiesData.kabupatenAcehBarat,
            CitiesData.kotaLangsa,
            CitiesData.kabupatenAcehTengah,
            CitiesData.kabupatenGayoLues,
            CitiesData.kotaSubulussalam,
            CitiesData.kabupatenAcehUtara,
            CitiesData.kotaSabang,
            CitiesData.kotaLhokseumawe,
            CitiesData.kabupatenPidieJaya,
            CitiesData.kabupatenPidie,
            CitiesData.kabupatenAcehSingkil,
            CitiesData.kabupatenSimeulue,
            CitiesData.kabupatenAcehSelatan,
            CitiesData.kabupatenAcehTamiang,
            CitiesData.kabupatenNaganRaya,
            CitiesData.kabupatenAcehJaya,
            CitiesData.kabupatenAcehTenggara,
            CitiesData.kabupatenBenerMeriah,
            CitiesData.kabupatenBireuen,
        };

        public static List<CityData> riau = new List<CityData> {
            CitiesData.kotaPekanbaru,
            CitiesData.kabupatenIndragiriHulu,
            CitiesData.kabupatenPelalawan,
            CitiesData.kabupatenBengkalis,
            CitiesData.kabupatenKampar,
            CitiesData.kabupatenIndragiriHilir,
            CitiesData.kotaDumai,
            CitiesData.kabupatenKuantanSingingi,
            CitiesData.kabupatenRokanHulu,
            CitiesData.kabupatenRokanHilir,
            CitiesData.kabupatenSiak,
            CitiesData.kabupatenKepulauanMeranti,
        };

        public static List<CityData> bangkaBelitung = new List<CityData> {
            CitiesData.kotaPangkalPinang,
            CitiesData.kabupatenBangka,
            CitiesData.kabupatenBangkaTengah,
            CitiesData.kabupatenBangkaBarat,
            CitiesData.kabupatenBangkaSelatan,
            CitiesData.kabupatenBelitung,
            CitiesData.kabupatenBelitungTimur,
        };

        public static List<CityData> kepulauanRiau = new List<CityData> {
            CitiesData.kotaTanjungPinang,
            CitiesData.kabupatenBintan,
            CitiesData.kotaBatam,
            CitiesData.kabupatenKarimun,
            CitiesData.kabupatenLingga,
            CitiesData.kabupatenNatuna,
            CitiesData.kabupatenKepulauanAnambas,
        };
    }

    public class BaliNusaTenggara {
        public static List<CityData> NttPulauTimor = new List<CityData> {
            CitiesData.kotaKupang,
            CitiesData.kabupatenKupang,
            CitiesData.kabupatenTimorTengahSelatan,
            CitiesData.kabupatenTimorTengahUtara,
            CitiesData.kabupatenBelu,
            CitiesData.kabupatenSabuRaijua,
            CitiesData.kabupatenRoteNdao,
            CitiesData.kabupatenMalaka,
        };
        public static List<CityData> bali = new List<CityData> {
            CitiesData.kotaDenpasar,
            CitiesData.kabupatenBadung,
            CitiesData.kabupatenTabanan,
            CitiesData.kabupatenGianyar,
            CitiesData.kabupatenKlungkung,
            CitiesData.kabupatenBangli,
            CitiesData.kabupatenKarangasem,
            CitiesData.kabupatenBuleleng,
            CitiesData.kabupatenJembrana,
        };
        public static List<CityData> NtbPulauLombok = new List<CityData> {
            CitiesData.kotaMataram,
            CitiesData.kabupatenLombokUtara,
            CitiesData.kabupatenLombokBarat,
            CitiesData.kabupatenLombokTimur,
            CitiesData.kabupatenLombokTengah,
        };
        public static List<CityData> NtbPulauSumbawa = new List<CityData> {
            CitiesData.kabupatenSumbawa,
            CitiesData.kabupatenSumbawaBarat,
            CitiesData.kotaBima,
            CitiesData.kabupatenDompu,
            CitiesData.kabupatenBima,
        };
        public static List<CityData> NttPulauFlores = new List<CityData> {
            CitiesData.kabupatenEnde,
            CitiesData.kabupatenSikka,
            CitiesData.kabupatenFloresTimur,
            CitiesData.kabupatenNgada,
            CitiesData.kabupatenManggarai,
            CitiesData.kabupatenLembata,
            CitiesData.kabupatenManggaraiBarat,
            CitiesData.kabupatenNageko,
            CitiesData.kabupatenAlor,
            CitiesData.kabupatenManggaraiTimur,
        };
        public static List<CityData> NttPulauSumba = new List<CityData> {
            CitiesData.kabupatenSumbaTimur,
            CitiesData.kabupatenSumbaBarat,
            CitiesData.kabupatenSumbaBaratDaya,
            CitiesData.kabupatenSumbaTengah,
        };
    }

    public class Kalimantan {
        public static List<CityData> kalimantanSelatan = new List<CityData> {
            CitiesData.kotaBanjarmasin,
            CitiesData.kabupatenBanjar,
            CitiesData.kabupatenHuluSungaiSelatan,
            CitiesData.kabupatenHuluSungaiTengah,
            CitiesData.kabupatenHuluSungaiUtara,
            CitiesData.kabupatenKotaBaru,
            CitiesData.kabupatenTabalong,
            CitiesData.kabupatenTapin,
            CitiesData.kabupatenTanahLaut,
            CitiesData.kabupatenBaritoKuala,
            CitiesData.kotaBanjarbaru,
            CitiesData.kabupatenBalangan,
            CitiesData.kabupatenTanahBumbu,
        };
        public static List<CityData> kalimantanBarat = new List<CityData> {
            CitiesData.kotaPontianak,
            CitiesData.kabupatenMempawah,
            CitiesData.kotaSingkawang,
            CitiesData.kabupatenSanggau,
            CitiesData.kabupatenSintang,
            CitiesData.kabupatenKapuasHulu,
            CitiesData.kabupatenKetapang,
            CitiesData.kabupatenMelawi,
            CitiesData.kabupatenBengkayang,
            CitiesData.kabupatenLandak,
            CitiesData.kabupatenKubuRaya,
            CitiesData.kabupatenSambas,
            CitiesData.kabupatenSekadau,
            CitiesData.kabupatenKayonUtara,
        };
        public static List<CityData> kalimantanTengah = new List<CityData> {
            CitiesData.kotaPalangkaraya,
            CitiesData.kabupatenKapuas,
            CitiesData.kabupatenBaritoSelatan,
            CitiesData.kabupatenBaritoUtara,
            CitiesData.kabupatenKotawaringinTimur,
            CitiesData.kabupatenKotawaringinBarat,
            CitiesData.kabupatenGunungMas,
            CitiesData.kabupatenPulangPisau,
            CitiesData.kabupatenBaritoTimur,
            CitiesData.kabupatenMurungRaya,
            CitiesData.kabupatenKatingan,
            CitiesData.kabupatenSeruyan,
            CitiesData.kabupatenLamandau,
            CitiesData.kabupatenSukamara,
        };
        public static List<CityData> kalimantanTimur = new List<CityData> {
            CitiesData.kotaBalikpapan,
            CitiesData.kotaSamarinda,
            CitiesData.kabupatenKutaiKartanegara,
            CitiesData.kotaBontang,
            CitiesData.kabupatenPaser,
            CitiesData.kabupatenBerau,
            CitiesData.kabupatenKutaiTimur,
            CitiesData.kabupatenKutaiBarat,
            CitiesData.kabupatenMahakamUlu,
            CitiesData.kabupatenPenajamPaserUtara,
        };
        public static List<CityData> kalimantanUtara = new List<CityData> {
            CitiesData.kabupatenBulungan,
            CitiesData.kotaTarakan,
            CitiesData.kabupatenTanaTidung,
            CitiesData.kabupatenNunukan,
            CitiesData.kabupatenMalinau,
        };
    }

    public class GorontaloSulawesi {
        public static List<CityData> sulawesiUtara = new List<CityData> {
            CitiesData.kotaManado,
            CitiesData.kabupatenMinahasa,
            CitiesData.kotaBitung,
            CitiesData.kabupatenBolaangMongodow,
            CitiesData.kabupatenMinahasaSelatan,
            CitiesData.kabupatenMinahasaUtara,
            CitiesData.kotaTomohon,
            CitiesData.kabupatenBolaangMongondowUtara,
            CitiesData.kabupatenMinahasaTenggara,
            CitiesData.kotaKotamubagu,
            CitiesData.kabupatenBolaangMongondowTimur,
            CitiesData.kabupatenBolaangMongondowSelatan,
        };
        public static List<CityData> sulawesiBarat = new List<CityData> {
            CitiesData.kabupatenMamuju,
            CitiesData.kabupatenMajene,
            CitiesData.kabupatenPolewaliMandar,
            CitiesData.kabupatenMamasa,
            CitiesData.kabupatenPasangkayu,
            CitiesData.kabupatenMamujuTengah,
        };
        public static List<CityData> sulawesiSelatanSelatan = new List<CityData> {
            CitiesData.kotaMakassar,
            CitiesData.kabupatenGowa,
            CitiesData.kabupatenTakalar,
            CitiesData.kabupatenMaros,
            CitiesData.kabupatenPangkajene,
            CitiesData.kabupatenBantaeng,
            CitiesData.kabupatenJeneponto,
            CitiesData.kabupatenBulukumba,
            CitiesData.kabupatenSelayar,
        };
        public static List<CityData> sulawesiUtaraKepulauan = new List<CityData> {
            CitiesData.kabupatenKepulauanSangihe,
            CitiesData.kabupatenKepulauanTalaud,
            CitiesData.kabupatenKepulauanSitaro,
        };
        public static List<CityData> gorontalo = new List<CityData> {
            CitiesData.kotaGorontalo,
            CitiesData.kabupatenGorontalo,
            CitiesData.kabupatenBoalemo,
            CitiesData.kabupatenPohuwato,
            CitiesData.kabupatenBoneBolango,
            CitiesData.kabupatenGorontaloUtara,
        };
        public static List<CityData> sulawesiTengah = new List<CityData> {
            CitiesData.kotaPalu,
            CitiesData.kabupatenDonggala,
            CitiesData.kabupatenBanggai,
            CitiesData.kabupatenToliToli,
            CitiesData.kabupatenPoso,
            CitiesData.kabupatenBuol,
            CitiesData.kabupatenMorowali,
            CitiesData.kabupatenBanggaiKepulauan,
            CitiesData.kabupatenParigiMoutong,
            CitiesData.kabupatenTojoUnaUna,
            CitiesData.kabupatenSigi,
            CitiesData.kabupatenBanggaiLaut,
            CitiesData.kabupatenMorowaliUtara,
        };
        public static List<CityData> sulawesiSelatanUtara = new List<CityData> {
            CitiesData.kotaParepare,
            CitiesData.kabupatenBarru,
            CitiesData.kabupatenSidenrengRappang,
            CitiesData.kabupatenPinrang,
            CitiesData.kotaPalopo,
            CitiesData.kabupatenLuwu,
            CitiesData.kabupatenLuwuTimur,
            CitiesData.kabupatenLuwuUtara,
            CitiesData.kabupatenEnrekang,
            CitiesData.kabupatenTanaToraja,
            CitiesData.kabupatenTorajaUtara,
        };
        public static List<CityData> sulawesiTenggara = new List<CityData> {
            CitiesData.kabupatenKonawe,
            CitiesData.kabupatenKolaka,
            CitiesData.kabupatenButon,
            CitiesData.kabupatenMuna,
            CitiesData.kotaKendari,
            CitiesData.kotaBaubau,
            CitiesData.kabupatenKonaweSelatan,
            CitiesData.kabupatenKolakaUtara,
            CitiesData.kabupatenBombana,
            CitiesData.kabupatenWakatobi,
            CitiesData.kabupatenKonaweUtara,
            CitiesData.kabupatenButonUtara,
            CitiesData.kabupatenKonaweKepulauan,
            CitiesData.kabupatenMunaBarat,
            CitiesData.kabupatenKolakaTimur,
            CitiesData.kabupatenButonSelatan,
            CitiesData.kabupatenButonTengah,
        };
        public static List<CityData> sulawesiSelatanTengah = new List<CityData> {
            CitiesData.kabupatenBone,
            CitiesData.kabupatenWajo,
            CitiesData.kabupatenSoppeng,
            CitiesData.kabupatenSinjai,
        };
    }

    public class MalukuPapua {
        public static List<CityData> maluku = new List<CityData> {
            CitiesData.kotaAmbon,
            CitiesData.kabupatenMalukuTengah,
            CitiesData.kabupatenMalukuTenggara,
            CitiesData.kabupatenBuru,
            CitiesData.kabupatenKepulauanTanimbar,
            CitiesData.kabupatenKepulauanAru,
            CitiesData.kabupatenSeramBarat,
            CitiesData.kabupatenSeramTimur,
            CitiesData.kotaTual,
            CitiesData.kabupatenMalukuBaratDaya,
            CitiesData.kabupatenBuruSelatan,
        };
        public static List<CityData> malukuUtara = new List<CityData> {
            CitiesData.kotaTernate,
            CitiesData.kotaTidore,
            CitiesData.kabupatenTaliabu,
            CitiesData.kabupatenMorotai,
            CitiesData.kabupatenHalmaheraBarat,
            CitiesData.kabupatenHalmaheraUtara,
            CitiesData.kabupatenHalmaheraSelatan,
            CitiesData.kabupatenSula,
            CitiesData.kabupatenHalmaheraTengah,
            CitiesData.kabupatenHalmaheraTimur,
        };
        // Actual data name: Papua Selatan, Tengah, & Pegunungan
        public static List<CityData> papuaSelatan = new List<CityData> {
            CitiesData.kotaJayapura,
            CitiesData.kabupatenJayawijaya,
            CitiesData.kabupatenBiakNumfor,
            CitiesData.kabupatenMimika,
            CitiesData.kabupatenPaniai,
            CitiesData.kabupatenMerauke,
            CitiesData.kabupatenJayapura,
            CitiesData.kabupatenNabire,
            CitiesData.kabupatenYapen,
            CitiesData.kabupatenWaropen,
            CitiesData.kabupatenPuncakJaya,
            CitiesData.kabupatenKeerom,
            CitiesData.kabupatenSarmi,
            CitiesData.kabupatenMappi,
            CitiesData.kabupatenSupiori,
            CitiesData.kabupatenBovenGigoel,
            CitiesData.kabupatenMamberanoRaya,
            CitiesData.kabupatenTolikara,
        };
        // Actual data name: Papua Barat & Barat Daya
        public static List<CityData> papuaBarat = new List<CityData> {
            CitiesData.kabupatenSorong,
            CitiesData.kabupatenTelukBintuni,
            CitiesData.kabupatenPegununganArfak,
            CitiesData.kabupatenFakfak,
            CitiesData.kabupatenManokwari,
            CitiesData.kabupatenKaimana,
            CitiesData.kabupatenManokwariSelatan,
            CitiesData.kabupatenTambrauw,
            CitiesData.kabupatenRajaAmpat,
            CitiesData.kotaSorong,
            CitiesData.kabupatenSorongSelatan,
            CitiesData.kabupatenMaybrat,
            CitiesData.kabupatenTelukWondama,
        };
    }
}
