// Template at the bottom of the file

[System.Serializable] 
public struct CityData {
    /// Nama kota
    public string cityName;
    /// Nama wilayah
    public string areaName;
    /// Kode wilayah
    public string areaCode;
    /// Possible letter to make up the rear letters
    public string rearLetters;
    /// Invert the default "X*" format of rear letters
    public bool isInvertedRearLetters;
};

[System.Serializable]
public class CitiesData {
    ///////////////////////////////////////////////////////////////////////
    // --------------------------------------------------------- JAWA
    ///////////////////////////////////////////////////////////////////////
    public static CityData kotaSerang = new CityData {
        cityName = "Kota Serang",
        areaName = "Banten",
        areaCode = "A",
        rearLetters = "ABCD",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSerang = new CityData {
        cityName = "Kabupaten Serang",
        areaName = "Banten",
        areaCode = "A",
        rearLetters = "EFGHI",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPandeglang = new CityData {
        cityName = "Kabupaten Pandeglang",
        areaName = "Banten",
        areaCode = "A",
        rearLetters = "JKLM",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLebak = new CityData {
        cityName = "Kabupaten Lebak",
        areaName = "Banten",
        areaCode = "A",
        rearLetters = "NOPQ",
        isInvertedRearLetters = false,
    };
    public static CityData kotaCilegon = new CityData {
        cityName = "Kota Cilegon",
        areaName = "Banten",
        areaCode = "A",
        rearLetters = "RSTU",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTangerangBanten = new CityData {
        cityName = "Kabupaten Tangerang",
        areaName = "Banten",
        areaCode = "A",
        rearLetters = "VWXYZ",
        isInvertedRearLetters = false,
    };
    public static CityData kaJakartaPusat = new CityData {
        cityName = "Kota Administrasi Jakarta Pusat",
        areaName = "Jakarta Raya",
        areaCode = "B",
        rearLetters = "AP",
        isInvertedRearLetters = false,
    };
    public static CityData kaJakartaBarat = new CityData {
        cityName = "Kota Administrasi Jakarta Barat",
        areaName = "Jakarta Raya",
        areaCode = "B",
        rearLetters = "BH",
        isInvertedRearLetters = false,
    };
    public static CityData kotaTangerang = new CityData {
        cityName = "Kota Tangerang",
        areaName = "Jakarta Raya",
        areaCode = "B",
        rearLetters = "CLOV",
        isInvertedRearLetters = false,
    };
    public static CityData kaJakartaSelatan = new CityData {
        cityName = "Kota Administrasi Jakarta Selatan",
        areaName = "Jakarta Raya",
        areaCode = "B",
        rearLetters = "DS",
        isInvertedRearLetters = false,
    };
    public static CityData kotaDepok = new CityData {
        cityName = "Kota Depok",
        areaName = "Jakarta Raya",
        areaCode = "B",
        rearLetters = "EZ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBekasi = new CityData {
        cityName = "Kota Bekasi",
        areaName = "Jakarta Raya",
        areaCode = "B",
        rearLetters = "FMX",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTangerangJakarta = new CityData {
        cityName = "Kabupaten Tangerang",
        areaName = "Jakarta Raya",
        areaCode = "B",
        rearLetters = "GJ",
        isInvertedRearLetters = false,
    };
    public static CityData kotaBekasi = new CityData {
        cityName = "Kota Bekasi",
        areaName = "Jakarta Raya",
        areaCode = "B",
        rearLetters = "KY",
        isInvertedRearLetters = false,
    };
    public static CityData kotaTangerangSelatan = new CityData {
        cityName = "Kota Tangerang Selatan",
        areaName = "Jakarta Raya",
        areaCode = "B",
        rearLetters = "NW",
        isInvertedRearLetters = false,
    };
    public static CityData kaJakartaUtara = new CityData {
        cityName = "Kota Administrasi Jakarta Utara",
        areaName = "Jakarta Raya",
        areaCode = "B",
        rearLetters = "QU",
        isInvertedRearLetters = false,
    };
    public static CityData kaJakartaTimur = new CityData {
        cityName = "Kota Administrasi Jakarta Timur",
        areaName = "Jakarta Raya",
        areaCode = "B",
        rearLetters = "RT",
        isInvertedRearLetters = false,
    };
    public static CityData kotaBandung = new CityData {
        cityName = "Kota Bandung",
        areaName = "Bandung Raya",
        areaCode = "D",
        rearLetters = "ABCDEFGHIJKLMNOPQR",
        isInvertedRearLetters = false,
    };
    public static CityData kotaCimahi = new CityData {
        cityName = "Kota Cimahi",
        areaName = "Bandung Raya",
        areaCode = "D",
        rearLetters = "ST",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBandungBarat = new CityData {
        cityName = "Kabupaten Bandung Barat",
        areaName = "Bandung Raya",
        areaCode = "D",
        rearLetters = "UX",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBandung = new CityData {
        cityName = "Kabupaten Bandung",
        areaName = "Bandung Raya",
        areaCode = "D",
        rearLetters = "VWYZ",
        isInvertedRearLetters = false,
    };
    public static CityData kotaCirebon = new CityData {
        cityName = "Kota Cirebon",
        areaName = "Cirebon Raya",
        areaCode = "E",
        rearLetters = "ABCDEFG",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenCirebon = new CityData {
        cityName = "Kabupaten Cirebon",
        areaName = "Cirebon Raya",
        areaCode = "E",
        rearLetters = "HIJKLMNO",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenIndramayu = new CityData {
        cityName = "Kabupaten Indramayu",
        areaName = "Cirebon Raya",
        areaCode = "E",
        rearLetters = "PQRST",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMajalengka = new CityData {
        cityName = "Kabupaten Majalengka",
        areaName = "Cirebon Raya",
        areaCode = "E",
        rearLetters = "UVWX",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKuningan = new CityData {
        cityName = "Kabupaten Kuningan",
        areaName = "Cirebon Raya",
        areaCode = "E",
        rearLetters = "YZ",
        isInvertedRearLetters = false,
    };
    public static CityData kotaBogor = new CityData {
        cityName = "Kota Bogor",
        areaName = "Bogor Raya",
        areaCode = "F",
        rearLetters = "ABCDE",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBogor = new CityData {
        cityName = "Kabupaten Bogor",
        areaName = "Bogor Raya",
        areaCode = "F",
        rearLetters = "FGHIJKLMNPR",
        isInvertedRearLetters = false,
    };
    public static CityData kotaSukabumi = new CityData {
        cityName = "Kota Sukabumi",
        areaName = "Bogor Raya",
        areaCode = "F",
        rearLetters = "OST",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSukabumi = new CityData {
        cityName = "Kabupaten Sukabumi",
        areaName = "Bogor Raya",
        areaCode = "F",
        rearLetters = "QUV",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenCianjur = new CityData {
        cityName = "Kabupaten Cianjur",
        areaName = "Bogor Raya",
        areaCode = "F",
        rearLetters = "WXYZ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPurwakarta = new CityData {
        cityName = "Kabupaten Purwakarta",
        areaName = "Purwakarta",
        areaCode = "T",
        rearLetters = "ABCDIJOQ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKarawang = new CityData {
        cityName = "Kabupaten Karawang",
        areaName = "Purwakarta",
        areaCode = "T",
        rearLetters = "DEFGHKLMNPRS",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSubang = new CityData {
        cityName = "Kabupaten Subang",
        areaName = "Purwakarta",
        areaCode = "T",
        rearLetters = "TUVWXYZ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSumedang = new CityData {
        cityName = "Kabupaten Sumedang",
        areaName = "Tasikmalaya",
        areaCode = "Z",
        rearLetters = "ABC",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenGarut = new CityData {
        cityName = "Kabupaten Garut",
        areaName = "Tasikmalaya",
        areaCode = "Z",
        rearLetters = "DEFG",
        isInvertedRearLetters = false,
    };
    public static CityData kotaTasikmalaya = new CityData {
        cityName = "Kota Tasikmalaya",
        areaName = "Tasikmalaya",
        areaCode = "Z",
        rearLetters = "HIJKLM",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTasikmalaya = new CityData {
        cityName = "Kabupaten Tasikmalaya",
        areaName = "Tasikmalaya",
        areaCode = "Z",
        rearLetters = "NOPQRS",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenCiamis = new CityData {
        cityName = "Kabupaten Ciamis",
        areaName = "Tasikmalaya",
        areaCode = "Z",
        rearLetters = "TVW",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPangandaran = new CityData {
        cityName = "Kabupaten Pangandaran",
        areaName = "Tasikmalaya",
        areaCode = "Z",
        rearLetters = "U",
        isInvertedRearLetters = false,
    };
    public static CityData kotaBanjar = new CityData {
        cityName = "Kota Banjar",
        areaName = "Tasikmalaya",
        areaCode = "Z",
        rearLetters = "XYZ",
        isInvertedRearLetters = false,
    };
    public static CityData kotaPekalongan = new CityData {
        cityName = "Kota Pekalongan",
        areaName = "Pekalongan",
        areaCode = "G",
        rearLetters = "AHS",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenPekalongan = new CityData {
        cityName = "Kabupaten Pekalongan",
        areaName = "Pekalongan",
        areaCode = "G",
        rearLetters = "BKOT",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenBatang = new CityData {
        cityName = "Kabupaten Batang",
        areaName = "Pekalongan",
        areaCode = "G",
        rearLetters = "CLVX",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenPemalang = new CityData {
        cityName = "Kabupaten Pemalang",
        areaName = "Pekalongan",
        areaCode = "G",
        rearLetters = "DIMW",
        isInvertedRearLetters = true,
    };
    public static CityData kotaTegal = new CityData {
        cityName = "Kota Tegal",
        areaName = "Pekalongan",
        areaCode = "G",
        rearLetters = "ENY",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenTegal = new CityData {
        cityName = "Kabupaten Tegal",
        areaName = "Pekalongan",
        areaCode = "G",
        rearLetters = "FPQZ",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenBrebes = new CityData {
        cityName = "Kabupaten Brebes",
        areaName = "Pekalongan",
        areaCode = "G",
        rearLetters = "GJRU",
        isInvertedRearLetters = true,
    };
    public static CityData kotaSemarang = new CityData {
        cityName = "Kota Semarang",
        areaName = "Semarang Raya",
        areaCode = "H",
        rearLetters = "AFGHPQRSWYZ",
        isInvertedRearLetters = true,
    };
    public static CityData kotaSalatiga = new CityData {
        cityName = "Kota Salatiga",
        areaName = "Semarang Raya",
        areaCode = "H",
        rearLetters = "BKOT",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenSemarang = new CityData {
        cityName = "Kabupaten Semarang",
        areaName = "Semarang Raya",
        areaCode = "H",
        rearLetters = "CILV",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenKendal = new CityData {
        cityName = "Kabupaten Kendal",
        areaName = "Semarang Raya",
        areaCode = "H",
        rearLetters = "DMU",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenDemak = new CityData {
        cityName = "Kabupaten Demak",
        areaName = "Semarang Raya",
        areaCode = "H",
        rearLetters = "EJN",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenPati = new CityData {
        cityName = "Kabupaten Pati",
        areaName = "Muria Raya",
        areaCode = "K",
        rearLetters = "AGHSU",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenKudus = new CityData {
        cityName = "Kabupaten Kudus",
        areaName = "Muria Raya",
        areaCode = "K",
        rearLetters = "BKORT",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenJepara = new CityData {
        cityName = "Kabupaten Jepara",
        areaName = "Muria Raya",
        areaCode = "K",
        rearLetters = "CLQV",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenRembang = new CityData {
        cityName = "Kabupaten Rembang",
        areaName = "Muria Raya",
        areaCode = "K",
        rearLetters = "DIMW",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenBlora = new CityData {
        cityName = "Kabupaten Blora",
        areaName = "Muria Raya",
        areaCode = "K",
        rearLetters = "ENXY",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenGrobogan = new CityData {
        cityName = "Kabupaten Grobogan",
        areaName = "Muria Raya",
        areaCode = "K",
        rearLetters = "FJPZ",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenBanyumas = new CityData {
        cityName = "Kabupaten Banyumas",
        areaName = "Banyumas",
        areaCode = "R",
        rearLetters = "AEGHJRS",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenCilacap = new CityData {
        cityName = "Kabupaten Cilacap",
        areaName = "Banyumas",
        areaCode = "R",
        rearLetters = "BFKNPT",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenPurbalingga = new CityData {
        cityName = "Kabupaten Purbalingga",
        areaName = "Banyumas",
        areaCode = "R",
        rearLetters = "CLQUVZ",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenBanjarnegara = new CityData {
        cityName = "Kabupaten Banjarnegara",
        areaName = "Banyumas",
        areaCode = "R",
        rearLetters = "DIMOWIY",
        isInvertedRearLetters = true,
    };
    public static CityData kotaMagelang = new CityData {
        cityName = "Kota Magelang",
        areaName = "Kedu",
        areaCode = "AA",
        rearLetters = "AHSU",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenMagelang = new CityData {
        cityName = "Kabupaten Magelang",
        areaName = "Kedu",
        areaCode = "AA",
        rearLetters = "BKOT",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenPurworejo = new CityData {
        cityName = "Kabupaten Purworejo",
        areaName = "Kedu",
        areaCode = "AA",
        rearLetters = "CLQV",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenKebumen = new CityData {
        cityName = "Kabupaten Kebumen",
        areaName = "Kedu",
        areaCode = "AA",
        rearLetters = "DJMW",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenTemanggung = new CityData {
        cityName = "Kabupaten Temanggung",
        areaName = "Kedu",
        areaCode = "AA",
        rearLetters = "ENY",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenWonosobo = new CityData {
        cityName = "Kabupaten Wonosobo",
        areaName = "Kedu",
        areaCode = "AA",
        rearLetters = "FPZ",
        isInvertedRearLetters = true,
    };
    public static CityData kotaYogyakarta = new CityData {
        cityName = "Kota Yogyakarta",
        areaName = "DI Yogyakarta",
        areaCode = "AB",
        rearLetters = "AHFIS",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenBantul = new CityData {
        cityName = "Kabupaten Bantul",
        areaName = "DI Yogyakarta",
        areaCode = "AB",
        rearLetters = "BGJKT",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenKulonProgo = new CityData {
        cityName = "Kabupaten Kulon Progo",
        areaName = "DI Yogyakarta",
        areaCode = "AB",
        rearLetters = "CLOPV",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenGunungkidul = new CityData {
        cityName = "Kabupaten Gunungkidul",
        areaName = "DI Yogyakarta",
        areaCode = "AB",
        rearLetters = "DMRW",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenSleman = new CityData {
        cityName = "Kabupaten Sleman",
        areaName = "DI Yogyakarta",
        areaCode = "AB",
        rearLetters = "ENQUXYZ",
        isInvertedRearLetters = true,
    };
    public static CityData kotaSurakarta = new CityData {
        cityName = "Kota Surakarta",
        areaName = "Solo Raya",
        areaCode = "AD",
        rearLetters = "AHSU",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenSukoharjo = new CityData {
        cityName = "Kabupaten Sukoharjo",
        areaName = "Solo Raya",
        areaCode = "AD",
        rearLetters = "BKOT",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenKlaten = new CityData {
        cityName = "Kabupaten Klaten",
        areaName = "Solo Raya",
        areaCode = "AD",
        rearLetters = "CJLQV",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenBoyolali = new CityData {
        cityName = "Kabupaten Boyolali",
        areaName = "Solo Raya",
        areaCode = "AD",
        rearLetters = "DMW",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenSragen = new CityData {
        cityName = "Kabupaten Sragen",
        areaName = "Solo Raya",
        areaCode = "AD",
        rearLetters = "ENY",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenKaranganyar = new CityData {
        cityName = "Kabupaten Karanganyar",
        areaName = "Solo Raya",
        areaCode = "AD",
        rearLetters = "FPZ",
        isInvertedRearLetters = true,
    };
    public static CityData kabupatenWonogiri = new CityData {
        cityName = "Kabupaten Wonogiri",
        areaName = "Solo Raya",
        areaCode = "AD",
        rearLetters = "GIR",
        isInvertedRearLetters = true,
    };
    public static CityData surabaya = new CityData {
        cityName = "Surabaya",
        areaName = "Surabaya",
        areaCode = "L",
        rearLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPamekasan = new CityData {
        cityName = "Kabupaten Pamekasan",
        areaName = "Madura",
        areaCode = "M",
        rearLetters = "ABCDEF",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBangkalan = new CityData {
        cityName = "Kabupaten Bangkalan",
        areaName = "Madura",
        areaCode = "M",
        rearLetters = "GHIJKLM",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSampang = new CityData {
        cityName = "Kabupaten Sampang",
        areaName = "Madura",
        areaCode = "M",
        rearLetters = "NOPQRS",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSumenep = new CityData {
        cityName = "Kabupaten Sumenep",
        areaName = "Madura",
        areaCode = "M",
        rearLetters = "TUVWXYZ",
        isInvertedRearLetters = false,
    };
    public static CityData kotaMalang = new CityData {
        cityName = "Kota Malang",
        areaName = "Pasuruan-Malang",
        areaCode = "N",
        rearLetters = "ABCD",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMalang = new CityData {
        cityName = "Kabupaten Malang",
        areaName = "Pasuruan-Malang",
        areaCode = "N",
        rearLetters = "EFGHI",
        isInvertedRearLetters = false,
    };
    public static CityData kotaBatu = new CityData {
        cityName = "Kota Batu",
        areaName = "Pasuruan-Malang",
        areaCode = "N",
        rearLetters = "JKL",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenProbolinggo = new CityData {
        cityName = "Kabupaten Probolinggo",
        areaName = "Pasuruan-Malang",
        areaCode = "N",
        rearLetters = "MN",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPasuruan = new CityData {
        cityName = "Kabupaten Pasuruan",
        areaName = "Pasuruan-Malang",
        areaCode = "N",
        rearLetters = "OST",
        isInvertedRearLetters = false,
    };
    public static CityData kotaProbolinggo = new CityData {
        cityName = "Kota Probolinggo",
        areaName = "Pasuruan-Malang",
        areaCode = "N",
        rearLetters = "PQR",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLumajang = new CityData {
        cityName = "Kabupaten Lumajang",
        areaName = "Pasuruan-Malang",
        areaCode = "N",
        rearLetters = "UYZ",
        isInvertedRearLetters = false,
    };
    public static CityData kotaPasuruan = new CityData {
        cityName = "Kota Pasuruan",
        areaName = "Pasuruan-Malang",
        areaCode = "N",
        rearLetters = "VWX",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBondowoso = new CityData {
        cityName = "Kabupaten Bondowoso",
        areaName = "Besuki",
        areaCode = "P",
        rearLetters = "ABC",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSitubondo = new CityData {
        cityName = "Kabupaten Situbondo",
        areaName = "Besuki",
        areaCode = "P",
        rearLetters = "DEF",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenJember = new CityData {
        cityName = "Kabupaten Jember",
        areaName = "Besuki",
        areaCode = "P",
        rearLetters = "GHIJKLMNOP",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBanyuwangi = new CityData {
        cityName = "Kabupaten Banyuwangi",
        areaName = "Besuki",
        areaCode = "P",
        rearLetters = "QRSTUVWXYZ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBojonegoro = new CityData {
        cityName = "Kabupaten Bojonegoro",
        areaName = "Bojonegoro",
        areaCode = "S",
        rearLetters = "ABCD",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTuban = new CityData {
        cityName = "Kabupaten Tuban",
        areaName = "Bojonegoro",
        areaCode = "S",
        rearLetters = "EFGHI",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLamongan = new CityData {
        cityName = "Kabupaten Lamongan",
        areaName = "Bojonegoro",
        areaCode = "S",
        rearLetters = "JKLM",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMojokerto = new CityData {
        cityName = "Kabupaten Mojokerto",
        areaName = "Bojonegoro",
        areaCode = "S",
        rearLetters = "NOPQR",
        isInvertedRearLetters = false,
    };
    public static CityData kotaMojokerto = new CityData {
        cityName = "Kota Mojokerto",
        areaName = "Bojonegoro",
        areaCode = "S",
        rearLetters = "STUV",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenJombang = new CityData {
        cityName = "Kabupaten Jombang",
        areaName = "Bojonegoro",
        areaCode = "S",
        rearLetters = "WXYZ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenGresik = new CityData {
        cityName = "Kabupaten Gresik",
        areaName = "Gresik-Sidoarjo",
        areaCode = "W",
        rearLetters = "ABCDEFGHIJKLM",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSidoarjo = new CityData {
        cityName = "Kabupaten Sidoarjo",
        areaName = "Gresik-Sidoarjo",
        areaCode = "W",
        rearLetters = "NOPQRSTUVWXYZ",
        isInvertedRearLetters = false,
    };
    public static CityData kotaMadiun = new CityData {
        cityName = "Kota Madiun",
        areaName = "Madiun",
        areaCode = "AE",
        rearLetters = "ABCD",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMadiun = new CityData {
        cityName = "Kabupaten Madiun",
        areaName = "Madiun",
        areaCode = "AE",
        rearLetters = "EFGHI",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenNgawi = new CityData {
        cityName = "Kabupaten Ngawi",
        areaName = "Madiun",
        areaCode = "AE",
        rearLetters = "JKLM",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMagetan = new CityData {
        cityName = "Kabupaten Magetan",
        areaName = "Madiun",
        areaCode = "AE",
        rearLetters = "NOPQR",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPonorogo = new CityData {
        cityName = "Kabupaten Ponorogo",
        areaName = "Madiun",
        areaCode = "AE",
        rearLetters = "STUVW",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPacitan = new CityData {
        cityName = "Kabupaten Pacitan",
        areaName = "Madiun",
        areaCode = "AE",
        rearLetters = "XYZ",
        isInvertedRearLetters = false,
    };
    public static CityData kotaKediri = new CityData {
        cityName = "Kota Kediri",
        areaName = "Kediri",
        areaCode = "AG",
        rearLetters = "ABC",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKediri = new CityData {
        cityName = "Kabupaten Kediri",
        areaName = "Kediri",
        areaCode = "AG",
        rearLetters = "DEFGHIJ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBlitar = new CityData {
        cityName = "Kabupaten Blitar",
        areaName = "Kediri",
        areaCode = "AG",
        rearLetters = "DEFGHIJ",
        isInvertedRearLetters = false,
    };
    public static CityData kotaBlitar = new CityData {
        cityName = "Kota Blitar",
        areaName = "Kediri",
        areaCode = "AG",
        rearLetters = "PQ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTulungagung = new CityData {
        cityName = "Kabupaten Tulungagung",
        areaName = "Kediri",
        areaCode = "AG",
        rearLetters = "RST",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenNganjuk = new CityData {
        cityName = "Kabupaten Nganjuk",
        areaName = "Kediri",
        areaCode = "AG",
        rearLetters = "UVWX",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTrenggalek = new CityData {
        cityName = "Kabupaten Trenggalek",
        areaName = "Kediri",
        areaCode = "AG",
        rearLetters = "YZ",
        isInvertedRearLetters = false,
    };

    ///////////////////////////////////////////////////////////////////////
    // --------------------------------------------------------- SUMATRA
    ///////////////////////////////////////////////////////////////////////
    public static CityData kotaPadang = new CityData {
        cityName = "Kota Padang",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "ABOQR",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLimaPuluhKota = new CityData {
        cityName = "Kabupaten Lima Puluh Kota",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "C",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPasaman = new CityData {
        cityName = "Kabupaten Pasaman",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "D",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTanahDatar = new CityData {
        cityName = "Kabupaten Tanah Datar",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "E",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPadangPariaman = new CityData {
        cityName = "Kabupaten Padang Pariaman",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "F",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPesisirSelatan = new CityData {
        cityName = "Kabupaten Pesisir Selatan",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "Z",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSolok = new CityData {
        cityName = "Kabupaten Solok",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "H",
        isInvertedRearLetters = false,
    };
    public static CityData kotaSawahlunto = new CityData {
        cityName = "Kota Sawahlunto",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "J",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSijunjung = new CityData {
        cityName = "Kabupaten Sijunjung",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "K",
        isInvertedRearLetters = false,
    };
    public static CityData kotaBukittinggi = new CityData {
        cityName = "Kota Bukittinggi",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "L",
        isInvertedRearLetters = false,
    };
    public static CityData kotaPayakumbuh = new CityData {
        cityName = "Kota Payakumbuh",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "M",
        isInvertedRearLetters = false,
    };
    public static CityData kotaPadangPanjang = new CityData {
        cityName = "Kota Padang Panjang",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "N",
        isInvertedRearLetters = false,
    };
    public static CityData kotaSolok = new CityData {
        cityName = "Kota Solok",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "P",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPasamanBarat = new CityData {
        cityName = "Kabupaten Pasaman Barat",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "S",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenAgam = new CityData {
        cityName = "Kabupaten Agam",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "T",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKepulauanMentawai = new CityData {
        cityName = "Kabupaten Kepulauan Mentawai",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "U",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenDharmasraya = new CityData {
        cityName = "Kabupaten Dharmasraya",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "V",
        isInvertedRearLetters = false,
    };
    public static CityData kotaPariaman = new CityData {
        cityName = "Kota Pariaman",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "W",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSolokSelatan = new CityData {
        cityName = "Kabupaten Solok Selatan",
        areaName = "Sumatra Barat",
        areaCode = "BA",
        rearLetters = "Y",
        isInvertedRearLetters = false,
    };


    public static CityData kotaSibolga = new CityData {
        cityName = "Kota Sibolga",
        areaName = "Sumatra Utara (Barat)",
        areaCode = "BB",
        rearLetters = "ALN",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTapanuliUtara = new CityData {
        cityName = "Kabupaten Tapanuli Utara",
        areaName = "Sumatra Utara (Barat)",
        areaCode = "BB",
        rearLetters = "B",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSamosir = new CityData {
        cityName = "Kabupaten Samosir",
        areaName = "Sumatra Utara (Barat)",
        areaCode = "BB",
        rearLetters = "C",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenHumbangHandusutan = new CityData {
        cityName = "Kabupaten Humbang Handusutan",
        areaName = "Sumatra Utara (Barat)",
        areaCode = "BB",
        rearLetters = "D",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenToba = new CityData {
        cityName = "Kabupaten Toba",
        areaName = "Sumatra Utara (Barat)",
        areaCode = "BB",
        rearLetters = "E",
        isInvertedRearLetters = false,
    };
    public static CityData kotaPadangSidempuan = new CityData {
        cityName = "Kota Padang Sidempuan",
        areaName = "Sumatra Utara (Barat)",
        areaCode = "BB",
        rearLetters = "FH",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTapanuliSelatan = new CityData {
        cityName = "Kabupaten Tapanuli Selatan",
        areaName = "Sumatra Utara (Barat)",
        areaCode = "BB",
        rearLetters = "G",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPadangLawasUtara = new CityData {
        cityName = "Kabupaten Padang Lawas Utara",
        areaName = "Sumatra Utara (Barat)",
        areaCode = "BB",
        rearLetters = "J",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPadangLawas = new CityData {
        cityName = "Kabupaten Padang Lawas",
        areaName = "Sumatra Utara (Barat)",
        areaCode = "BB",
        rearLetters = "K",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTapanuliTengah = new CityData {
        cityName = "Kabupaten Tapanuli Tengah",
        areaName = "Sumatra Utara (Barat)",
        areaCode = "BB",
        rearLetters = "M",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenNiasUtara = new CityData {
        cityName = "Kabupaten Nias Utara",
        areaName = "Sumatra Utara (Barat)",
        areaCode = "BB",
        rearLetters = "Q",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMandailingNatal = new CityData {
        cityName = "Kabupaten Mandailing Natal",
        areaName = "Sumatra Utara (Barat)",
        areaCode = "BB",
        rearLetters = "R",
        isInvertedRearLetters = false,
    };
    public static CityData kotaGunungsitoli = new CityData {
        cityName = "Kota Gunungsitoli",
        areaName = "Sumatra Utara (Barat)",
        areaCode = "BB",
        rearLetters = "T",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenNiasBarat = new CityData {
        cityName = "Kabupaten Nias Barat",
        areaName = "Sumatra Utara (Barat)",
        areaCode = "BB",
        rearLetters = "U",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenNias = new CityData {
        cityName = "Kabupaten Nias",
        areaName = "Sumatra Utara (Barat)",
        areaCode = "BB",
        rearLetters = "V",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenNiasSelatan = new CityData {
        cityName = "Kabupaten Nias Selatan",
        areaName = "Sumatra Utara (Barat)",
        areaCode = "BB",
        rearLetters = "W",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenDairi = new CityData {
        cityName = "Kabupaten Dairi",
        areaName = "Sumatra Utara (Barat)",
        areaCode = "BB",
        rearLetters = "Y",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPakpakBharat = new CityData {
        cityName = "Kabupaten Pakpak Bharat",
        areaName = "Sumatra Utara (Barat)",
        areaCode = "BB",
        rearLetters = "Z",
        isInvertedRearLetters = false,
    };
    public static CityData kotaBengkulu = new CityData {
        cityName = "Kota Bengkulu",
        areaName = "Bengkulu",
        areaCode = "BD",
        rearLetters = "ACIUV",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBengkuluSelatan = new CityData {
        cityName = "Kabupaten Bengkulu Selatan",
        areaName = "Bengkulu",
        areaCode = "BD",
        rearLetters = "BM",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBengkuluUtara = new CityData {
        cityName = "Kabupaten Bengkulu Utara",
        areaName = "Bengkulu",
        areaCode = "BD",
        rearLetters = "DS",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenRejangLebong = new CityData {
        cityName = "Kabupaten Rejang Lebong",
        areaName = "Bengkulu",
        areaCode = "BD",
        rearLetters = "FK",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKepahiang = new CityData {
        cityName = "Kabupaten Kepahiang",
        areaName = "Bengkulu",
        areaCode = "BD",
        rearLetters = "G",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLebong = new CityData {
        cityName = "Kabupaten Lebong",
        areaName = "Bengkulu",
        areaCode = "BD",
        rearLetters = "H",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMukoMuko = new CityData {
        cityName = "Kabupaten Muko Muko",
        areaName = "Bengkulu",
        areaCode = "BD",
        rearLetters = "N",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSeluma = new CityData {
        cityName = "Kabupaten Seluma",
        areaName = "Bengkulu",
        areaCode = "BD",
        rearLetters = "PR",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKaur = new CityData {
        cityName = "Kabupaten Kaur",
        areaName = "Bengkulu",
        areaCode = "BD",
        rearLetters = "W",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBengkuluTengah = new CityData {
        cityName = "Kabupaten Bengkulu Tengah",
        areaName = "Bengkulu",
        areaCode = "BD",
        rearLetters = "Y",
        isInvertedRearLetters = false,
    };
    public static CityData kotaBandarLampung = new CityData {
        cityName = "Kota Bandar Lampung",
        areaName = "Lampung",
        areaCode = "BE",
        rearLetters = "ABCY",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLampungSelatan = new CityData {
        cityName = "Kabupaten Lampung Selatan",
        areaName = "Lampung",
        areaCode = "BE",
        rearLetters = "DEO",
        isInvertedRearLetters = false,
    };
    public static CityData kotaMetro = new CityData {
        cityName = "Kota Metro",
        areaName = "Lampung",
        areaCode = "BE",
        rearLetters = "F",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLampungTengah = new CityData {
        cityName = "Kabupaten Lampung Tengah",
        areaName = "Lampung",
        areaCode = "BE",
        rearLetters = "GHI",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLampungUtara = new CityData {
        cityName = "Kabupaten Lampung Utara",
        areaName = "Lampung",
        areaCode = "BE",
        rearLetters = "JK",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMesuji = new CityData {
        cityName = "Kabupaten Mesuji",
        areaName = "Lampung",
        areaCode = "BE",
        rearLetters = "L",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLampungBarat = new CityData {
        cityName = "Kabupaten Lampung Barat",
        areaName = "Lampung",
        areaCode = "BE",
        rearLetters = "M",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLampungTimur = new CityData {
        cityName = "Kabupaten Lampung Timur",
        areaName = "Lamgpung",
        areaCode = "BE",
        rearLetters = "NP",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTulangBawangBarat = new CityData {
        cityName = "Kabupaten Tulang Bawang Barat",
        areaName = "Lampung",
        areaCode = "BE",
        rearLetters = "Q",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPesawaran = new CityData {
        cityName = "Kabupaten Pesawaran",
        areaName = "Lampung",
        areaCode = "BE",
        rearLetters = "R",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTulangBawang = new CityData {
        cityName = "Kabupaten Tulang Bawang",
        areaName = "Lampung",
        areaCode = "BE",
        rearLetters = "ST",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPringsewu = new CityData {
        cityName = "Kabupaten Pringsewu",
        areaName = "Lampung",
        areaCode = "BE",
        rearLetters = "U",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTanggamus = new CityData {
        cityName = "Kabupaten Tanggamus",
        areaName = "Lampung",
        areaCode = "BE",
        rearLetters = "VZ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenWayKanan = new CityData {
        cityName = "Kabupaten Way Kanan",
        areaName = "Lampung",
        areaCode = "BE",
        rearLetters = "W",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPesisirBarat = new CityData {
        cityName = "Kabupaten Pesisir Barat",
        areaName = "Lampung",
        areaCode = "BE",
        rearLetters = "X",
        isInvertedRearLetters = false,
    };
    public static CityData kotaPalembang = new CityData {
        cityName = "Kota Palembang",
        areaName = "Sumatra Selatan",
        areaCode = "BG",
        rearLetters = "AILMNUVXZ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMusiBanyuasin = new CityData {
        cityName = "Kabupaten Musi Banyuasin",
        areaName = "Sumatra Selatan",
        areaCode = "BG",
        rearLetters = "B",
        isInvertedRearLetters = false,
    };
    public static CityData kotaPrabumulih = new CityData {
        cityName = "Kota Prabumulih",
        areaName = "Sumatra Selatan",
        areaCode = "BG",
        rearLetters = "C",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMuaraEnim = new CityData {
        cityName = "Kabupaten Muara Enim",
        areaName = "Sumatra Selatan",
        areaCode = "BG",
        rearLetters = "D",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLahat = new CityData {
        cityName = "Kabupaten Lahat",
        areaName = "Sumatra Selatan",
        areaCode = "BG",
        rearLetters = "E",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenOganKomeringUlu = new CityData {
        cityName = "Kabupaten Ogan Komering Ulu",
        areaName = "Sumatra Selatan",
        areaCode = "BG",
        rearLetters = "F",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMusiRawas = new CityData {
        cityName = "Kabupaten Musi Rawas",
        areaName = "Sumatra Selatan",
        areaCode = "BG",
        rearLetters = "G",
        isInvertedRearLetters = false,
    };
    public static CityData kotaLubukLinggau = new CityData {
        cityName = "Kota Lubuk Linggau",
        areaName = "Sumatra Selatan",
        areaCode = "BG",
        rearLetters = "H",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBanyuasin = new CityData {
        cityName = "Kabupaten Banyuasin",
        areaName = "Sumatra Selatan",
        areaCode = "BG",
        rearLetters = "JR",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenOganKomeringIlir = new CityData {
        cityName = "Kabupaten Ogan Komering Ilir",
        areaName = "Sumatra Selatan",
        areaCode = "BG",
        rearLetters = "K",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPenukalAbabLematangIlir = new CityData {
        cityName = "Kabupaten Penukal Abab Lematang Ilir",
        areaName = "Sumatra Selatan",
        areaCode = "BG",
        rearLetters = "P",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMusiRawasUtara = new CityData {
        cityName = "Kabupaten Musi Rawas Utara",
        areaName = "Sumatra Utara",
        areaCode = "BG",
        rearLetters = "Q",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenEmpatLawang = new CityData {
        cityName = "Kabupaten Empat Lawang",
        areaName = "Sumatra Utara",
        areaCode = "BG",
        rearLetters = "S",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenOganIlir = new CityData {
        cityName = "Kabupaten Ogan Ilir",
        areaName = "Sumatra Selatan",
        areaCode = "BG",
        rearLetters = "T",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenOganKomeringUluSelatan = new CityData {
        cityName = "Kabupaten Ogan Komering Ulu Selatan",
        areaName = "Sumatra Selatan",
        areaCode = "BG",
        rearLetters = "O",
        isInvertedRearLetters = false,
    };
    public static CityData kotaPagaralam = new CityData {
        cityName = "Kota Pagaralam",
        areaName = "Sumatra Selatan",
        areaCode = "BG",
        rearLetters = "W",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenOganKomeringUluTimur = new CityData {
        cityName = "Kabupaten Ogan Komering Ulu Timur",
        areaName = "Sumatra Selatan",
        areaCode = "BG",
        rearLetters = "Y",
        isInvertedRearLetters = false,
    };
    public static CityData kotaJambi = new CityData {
        cityName = "Kota Jambi",
        areaName = "Jambi",
        areaCode = "BH",
        rearLetters = "AHLMNYZ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBatanghari = new CityData {
        cityName = "Kabupaten Batanghari",
        areaName = "Jambi",
        areaCode = "BH",
        rearLetters = "BV",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTebo = new CityData {
        cityName = "Kabupaten Tebo",
        areaName = "Jambi",
        areaCode = "BH",
        rearLetters = "CW",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKerinci = new CityData {
        cityName = "Kabupaten Kerinci",
        areaName = "Jambi",
        areaCode = "BH",
        rearLetters = "D",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTanjungJabungBarat = new CityData {
        cityName = "Kabupaten Tanjung jabung Barat",
        areaName = "Jambi",
        areaCode = "BH",
        rearLetters = "EO",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMerangin = new CityData {
        cityName = "Kabupaten Merangin",
        areaName = "Jambi",
        areaCode = "BH",
        rearLetters = "FPX",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMuaroJambi = new CityData {
        cityName = "Kabupaten Muaro Jambi",
        areaName = "Jambi",
        areaCode = "BH",
        rearLetters = "GI",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTanjungJabungTimur = new CityData {
        cityName = "Kabupaten Tanjung Jabung Timur",
        areaName = "Jambi",
        areaCode = "BH",
        rearLetters = "JT",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBungo = new CityData {
        cityName = "Kabupaten Bungo",
        areaName = "Jambi",
        areaCode = "BH",
        rearLetters = "KU",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSarolangun = new CityData {
        cityName = "Kabupaten Salorangun",
        areaName = "Jambi",
        areaCode = "BH",
        rearLetters = "QS",
        isInvertedRearLetters = false,
    };
    public static CityData kotaSungaiPenuh = new CityData {
        cityName = "Kota Sungai Penuh",
        areaName = "Jambi",
        areaCode = "BH",
        rearLetters = "R",
        isInvertedRearLetters = false,
    };
    public static CityData kotaMedan = new CityData {
        cityName = "Kota Medan",
        areaName = "Sumatra Utara (Timur)",
        areaCode = "BK",
        rearLetters = "ABCDEFGHIKL",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLabuhanbatuUtara = new CityData {
        cityName = "Kabupaten Labuhanbatu Utara",
        areaName = "Sumatra Utara (Timur)",
        areaCode = "BK",
        rearLetters = "J",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenDeliSerdang = new CityData {
        cityName = "Kabupaten Deli Serdang",
        areaName = "Sumatra Utara (Timur)",
        areaCode = "BK",
        rearLetters = "M",
        isInvertedRearLetters = false,
    };
    public static CityData kotaTebingTinggi = new CityData {
        cityName = "Kota Tebing Tinggi",
        areaName = "Sumatra Utara (Timur)",
        areaCode = "BK",
        rearLetters = "N",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBatubara = new CityData {
        cityName = "Kabupaten Batubara",
        areaName = "Sumatra Utara (Timur)",
        areaCode = "BK",
        rearLetters = "O",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLangkat = new CityData {
        cityName = "Kabupaten Langkat",
        areaName = "Sumatra Utara (Timur)",
        areaCode = "BK",
        rearLetters = "P",
        isInvertedRearLetters = false,
    };
    public static CityData kotaTanjungBalai = new CityData {
        cityName = "Kota Tanjung Balai",
        areaName = "Sumatra Utara (Timur)",
        areaCode = "BK",
        rearLetters = "Q",
        isInvertedRearLetters = false,
    };
    public static CityData kotaBinjai = new CityData {
        cityName = "Kota Binjai",
        areaName = "Sumatra Utara (Timur)",
        areaCode = "BK",
        rearLetters = "R",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKaro = new CityData {
        cityName = "Kabupaten Karo",
        areaName = "Sumatra Utara (Timur)",
        areaCode = "BK",
        rearLetters = "S",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSimalungun = new CityData {
        cityName = "Kabupaten Simalungun",
        areaName = "Sumatra Utara (Timur)",
        areaCode = "BK",
        rearLetters = "TU",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenAsahan = new CityData {
        cityName = "Kabupaten Asahan",
        areaName = "Sumatra Utara (Timur)",
        areaCode = "BK",
        rearLetters = "V",
        isInvertedRearLetters = false,
    };
    public static CityData kotaPematangSiantar = new CityData {
        cityName = "Kota Pematang Siantar",
        areaName = "Sumatra Utara (Timur)",
        areaCode = "BK",
        rearLetters = "W",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSerdangBegadai = new CityData {
        cityName = "Kabupaten Serdang Begadai",
        areaName = "Sumatra Utara (Timur)",
        areaCode = "BK",
        rearLetters = "X",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLabuhanbatu = new CityData {
        cityName = "Kabupaten Labuhanbatu",
        areaName = "Sumatra Utara (Timur)",
        areaCode = "BK",
        rearLetters = "Y",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLabuhanbatuSelatan = new CityData {
        cityName = "Kabupaten Labuhanbatu Selatan",
        areaName = "Sumatra Utara (Timur)",
        areaCode = "BK",
        rearLetters = "Z",
        isInvertedRearLetters = false,
    };
    public static CityData kotaBandaAceh = new CityData {
        cityName = "Kota Banda Aceh",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "AJ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenAcehBesar = new CityData {
        cityName = "Kabupaten Aceh Besar",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "BL",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenAcehBaratDaya = new CityData {
        cityName = "Kabupaten Aceh Barat Daya",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "C",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenAcehTimur = new CityData {
        cityName = "Kabupaten Aceh Timur",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "D",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenAcehBarat = new CityData {
        cityName = "Kabupaten Aceh Barat",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "E",
        isInvertedRearLetters = false,
    };
    public static CityData kotaLangsa = new CityData {
        cityName = "Kota Langsa",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "F",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenAcehTengah = new CityData {
        cityName = "Kabupaten Aceh Tengah",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "G",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenGayoLues = new CityData {
        cityName = "Kabupaten Gayo Lues",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "H",
        isInvertedRearLetters = false,
    };
    public static CityData kotaSubulussalam = new CityData {
        cityName = "Kota Subulussalam",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "I",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenAcehUtara = new CityData {
        cityName = "Kabupaten Aceh Utara",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "KQ",
        isInvertedRearLetters = false,
    };
    public static CityData kotaSabang = new CityData {
        cityName = "Kota Sabang",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "M",
        isInvertedRearLetters = false,
    };
    public static CityData kotaLhokseumawe = new CityData {
        cityName = "Kota Lhokseumawe",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "N",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPidieJaya = new CityData {
        cityName = "Kabupaten Pidie Jaya",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "O",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPidie = new CityData {
        cityName = "Kabupaten Pidie",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "P",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenAcehSingkil = new CityData {
        cityName = "Kabupaten Aceh Singkil",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "R",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSimeulue = new CityData {
        cityName = "Kabupaten Simeulue",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "S",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenAcehSelatan = new CityData {
        cityName = "Kabupaten Aceh Selatan",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "T",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenAcehTamiang = new CityData {
        cityName = "Kabupaten Aceh Tamiang",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "U",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenNaganRaya = new CityData {
        cityName = "Kabupaten Nagan Raya",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "V",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenAcehJaya = new CityData {
        cityName = "Kabupaten Aceh Jaya",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "W",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenAcehTenggara = new CityData {
        cityName = "Kabupaten Aceh Tenggara",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "X",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBenerMeriah = new CityData {
        cityName = "Kabupaten Bener Meriah",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "Y",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBireuen = new CityData {
        cityName = "Kabupaten Bireuen",
        areaName = "Aceh",
        areaCode = "BL",
        rearLetters = "Z",
        isInvertedRearLetters = false,
    };
    public static CityData kotaPekanbaru = new CityData {
        cityName = "Kota Pekanbaru",
        areaName = "Riau",
        areaCode = "BM",
        rearLetters = "AJLNOQTV",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenIndragiriHulu = new CityData {
        cityName = "Kabupaten Indragiri Hulu",
        areaName = "Riau",
        areaCode = "BM",
        rearLetters = "B",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPelalawan = new CityData {
        cityName = "Kabupaten Pelalawan",
        areaName = "Riau",
        areaCode = "BM",
        rearLetters = "CI",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBengkalis = new CityData {
        cityName = "Kabupaten Bengkalis",
        areaName = "Riau",
        areaCode = "BM",
        rearLetters = "DE",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKampar = new CityData {
        cityName = "Kabupaten Kampar",
        areaName = "Riau",
        areaCode = "BM",
        rearLetters = "FZ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenIndragiriHilir = new CityData {
        cityName = "Kabupaten Indragiri Hilir",
        areaName = "Riau",
        areaCode = "BM",
        rearLetters = "G",
        isInvertedRearLetters = false,
    };
    public static CityData kotaDumai = new CityData {
        cityName = "Kota Dumai",
        areaName = "Riau",
        areaCode = "BM",
        rearLetters = "HR",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKuantanSingingi = new CityData {
        cityName = "Kabupaten Kuantan Singingi",
        areaName = "Riau",
        areaCode = "BM",
        rearLetters = "KZ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenRokanHulu = new CityData {
        cityName = "Kabupaten Rokan Hulu",
        areaName = "Riau",
        areaCode = "BM",
        rearLetters = "MU",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenRokanHilir = new CityData {
        cityName = "Kabupaten Rokan Hilir",
        areaName = "Riau",
        areaCode = "BM",
        rearLetters = "PW",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSiak = new CityData {
        cityName = "Kabupaten Siak",
        areaName = "Riau",
        areaCode = "BM",
        rearLetters = "SY",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKepulauanMeranti = new CityData {
        cityName = "Kabupaten Kepulauan Meranti",
        areaName = "Riau",
        areaCode = "BM",
        rearLetters = "J",
        isInvertedRearLetters = false,
    };
    public static CityData kotaPangkalPinang = new CityData {
        cityName = "Kota Pangkal Pinang",
        areaName = "Bangka Belitung",
        areaCode = "BN",
        rearLetters = "AP",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBangka = new CityData {
        cityName = "Kabupaten Bangka",
        areaName = "Bangka Belitung",
        areaCode = "BN",
        rearLetters = "BQ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBangkaTengah = new CityData {
        cityName = "Kabupaten Bangka Tengah",
        areaName = "Bangka Belitung",
        areaCode = "BN",
        rearLetters = "CT",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBangkaBarat = new CityData {
        cityName = "Kabupaten Bangka Barat",
        areaName = "Bangka Belitung",
        areaCode = "BN",
        rearLetters = "DR",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBangkaSelatan = new CityData {
        cityName = "Kabupaten Bangka Selatan",
        areaName = "Bangka Belitung",
        areaCode = "BN",
        rearLetters = "EV",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBelitung = new CityData {
        cityName = "Kabupaten Belitung",
        areaName = "Bangka Belitung",
        areaCode = "BN",
        rearLetters = "FW",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBelitungTimur = new CityData {
        cityName = "Kabupaten Belitung Timur",
        areaName = "Bangka Belitung",
        areaCode = "BN",
        rearLetters = "GX",
        isInvertedRearLetters = false,
    };
    public static CityData kotaTanjungPinang = new CityData {
        cityName = "Kota Tanjung Pinang",
        areaName = "Kepulauan Riau",
        areaCode = "BP",
        rearLetters = "APTW",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBintan = new CityData {
        cityName = "Kabupaten Bintan",
        areaName = "Kepulauan Riau",
        areaCode = "BP",
        rearLetters = "B",
        isInvertedRearLetters = false,
    };
    public static CityData kotaBatam = new CityData {
        cityName = "Kota Batam",
        areaName = "Kepulauan Riau",
        areaCode = "BP",
        rearLetters = "CDEFGHIJMOQRUVXYZ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKarimun = new CityData {
        cityName = "Kabupaten Karimun",
        areaName = "Kepulauan Riau",
        areaCode = "BP",
        rearLetters = "K",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLingga = new CityData {
        cityName = "Kabupaten Lingga",
        areaName = "Kepulauan Riau",
        areaCode = "BP",
        rearLetters = "L",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenNatuna = new CityData {
        cityName = "Kabupaten Natuna",
        areaName = "Kepulauan Riau",
        areaCode = "BP",
        rearLetters = "N",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKepulauanAnambas = new CityData {
        cityName = "Kabupaten Kepulauan Anambas",
        areaName = "Kepulauan Riau",
        areaCode = "BP",
        rearLetters = "S",
        isInvertedRearLetters = false,
    };

    ///////////////////////////////////////////////////////////////////////
    // --------------------------------------------------------- BALI NTT
    ///////////////////////////////////////////////////////////////////////
    public static CityData kotaKupang = new CityData {
        cityName = "Kota Kupang",
        areaName = "NTT (Pulau Timor)",
        areaCode = "DH",
        rearLetters = "AHK",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKupang = new CityData {
        cityName = "Kabupaten Kupang",
        areaName = "NTT (Pulau Timor)",
        areaCode = "DH",
        rearLetters = "BN",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTimorTengahSelatan = new CityData {
        cityName = "Kabupaten Timor Tengah Selatan",
        areaName = "NTT (Pulau Timor)",
        areaCode = "DH",
        rearLetters = "C",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTimorTengahUtara = new CityData {
        cityName = "Kabupaten Timor Tengah Utara",
        areaName = "NTT (Pulau Timor)",
        areaCode = "DH",
        rearLetters = "DM",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBelu = new CityData {
        cityName = "Kabupaten Belu",
        areaName = "NTT (Pulau Timor)",
        areaCode = "DH",
        rearLetters = "ET",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSabuRaijua = new CityData {
        cityName = "Kabupaten Sabu Raijua",
        areaName = "NTT (Pulau Timor)",
        areaCode = "DH",
        rearLetters = "F",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenRoteNdao = new CityData {
        cityName = "Kabupaten Rote Ndao",
        areaName = "NTT (Pulau Timor)",
        areaCode = "DH",
        rearLetters = "G",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMalaka = new CityData {
        cityName = "Kabupaten Malaka",
        areaName = "NTT (Pulau Timor)",
        areaCode = "DH",
        rearLetters = "J",
        isInvertedRearLetters = false,
    };
    public static CityData kotaDenpasar = new CityData {
        cityName = "Kota Denpasar",
        areaName = "Bali",
        areaCode = "DK",
        rearLetters = "ABCDEIX",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBadung = new CityData {
        cityName = "Kabupaten Badung",
        areaName = "Bali",
        areaCode = "DK",
        rearLetters = "FJOQ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTabanan = new CityData {
        cityName = "Kabupaten Tabanan",
        areaName = "Bali",
        areaCode = "DK",
        rearLetters = "GH",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenGianyar = new CityData {
        cityName = "Kabupaten Gianyar",
        areaName = "Bali",
        areaCode = "DK",
        rearLetters = "KL",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKlungkung = new CityData {
        cityName = "Kabupaten Klungkung",
        areaName = "Bali",
        areaCode = "DK",
        rearLetters = "MN",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBangli = new CityData {
        cityName = "Kabupaten Bangli",
        areaName = "Bali",
        areaCode = "DK",
        rearLetters = "PR",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKarangasem = new CityData {
        cityName = "Kabupaten Karangasem",
        areaName = "Bali",
        areaCode = "DK",
        rearLetters = "ST",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBuleleng = new CityData {
        cityName = "Kabupaten Buleleng",
        areaName = "Bali",
        areaCode = "DK",
        rearLetters = "UV",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenJembrana = new CityData {
        cityName = "Kabupaten Jembrana",
        areaName = "Bali",
        areaCode = "DK",
        rearLetters = "WZ",
        isInvertedRearLetters = false,
    };
    public static CityData kotaMataram = new CityData {
        cityName = "Kota Mataram",
        areaName = "NTB (Pulau Lombok)",
        areaCode = "DR",
        rearLetters = "ABCEFNOPRX",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLombokUtara = new CityData {
        cityName = "Kabupaten Lombok Utara",
        areaName = "NTB (Pulau Lombok)",
        areaCode = "DR",
        rearLetters = "DGM",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLombokBarat = new CityData {
        cityName = "Kabupaten Lombok Barat",
        areaName = "NTB (Pulau Lombok)",
        areaCode = "DR",
        rearLetters = "HJKTW",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLombokTimur = new CityData {
        cityName = "Kabupaten Lombok Timur",
        areaName = "NTB (Pulau Lombok)",
        areaCode = "DR",
        rearLetters = "LQY",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLombokTengah = new CityData {
        cityName = "Kabupaten Lombok Tengah",
        areaName = "NTB (Pulau Lombok)",
        areaCode = "DR",
        rearLetters = "SUVZ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSumbawa = new CityData {
        cityName = "Kabupaten Sumbawa",
        areaName = "NTB (Pulau Sumbawa)",
        areaCode = "EA",
        rearLetters = "ACDEFP",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSumbawaBarat = new CityData {
        cityName = "Kabupaten Sumbawa Barat",
        areaName = "NTB (Pulau Sumbawa)",
        areaCode = "EA",
        rearLetters = "HK",
        isInvertedRearLetters = false,
    };
    public static CityData kotaBima = new CityData {
        cityName = "Kota Bima",
        areaName = "NTB (Pulau Sumbawa)",
        areaCode = "EA",
        rearLetters = "LS",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenDompu = new CityData {
        cityName = "Kabupaten Dompu",
        areaName = "NTB (Pulau Sumbawa)",
        areaCode = "EA",
        rearLetters = "MQRT",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBima = new CityData {
        cityName = "Kabupaten Bima",
        areaName = "NTB (Pulau Sumbawa)",
        areaCode = "EA",
        rearLetters = "XY",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenEnde = new CityData {
        cityName = "Kabupaten Ende",
        areaName = "NTT (Pulau Flores)",
        areaCode = "EB",
        rearLetters = "A",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSikka = new CityData {
        cityName = "Kabupaten Sikka",
        areaName = "NTT (Pulau Flores)",
        areaCode = "EB",
        rearLetters = "B",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenFloresTimur = new CityData {
        cityName = "Kabupaten Flores Timur",
        areaName = "NTT (Pulau Flores)",
        areaCode = "EB",
        rearLetters = "C",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenNgada = new CityData {
        cityName = "Kabupaten Ngada",
        areaName = "NTT (Pulau Flores)",
        areaCode = "EB",
        rearLetters = "D",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenManggarai = new CityData {
        cityName = "Kabupaten Manggarai",
        areaName = "NTT (Pulau Flores)",
        areaCode = "EB",
        rearLetters = "E",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLembata = new CityData {
        cityName = "Kabupaten Lembata",
        areaName = "NTT (Pulau Flores)",
        areaCode = "EB",
        rearLetters = "F",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenManggaraiBarat = new CityData {
        cityName = "Kabupaten Manggarai Barat",
        areaName = "NTT (Pulau Flores)",
        areaCode = "EB",
        rearLetters = "G",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenNageko = new CityData {
        cityName = "Kabupaten Nageko",
        areaName = "NTT (Pulau Flores)",
        areaCode = "EB",
        rearLetters = "H",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenAlor = new CityData {
        cityName = "Kabupaten Alor",
        areaName = "NTT (Pulau Flores)",
        areaCode = "EB",
        rearLetters = "JK",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenManggaraiTimur = new CityData {
        cityName = "Kabupaten Manggarai Timur",
        areaName = "NTT (Pulau Flores)",
        areaCode = "EB",
        rearLetters = "P",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSumbaTimur = new CityData {
        cityName = "Kabupaten Sumba Timur",
        areaName = "NTT (Pulau Sumba)",
        areaCode = "ED",
        rearLetters = "A",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSumbaBarat = new CityData {
        cityName = "Kabupaten Sumba Barat",
        areaName = "NTT (Pulau Sumba)",
        areaCode = "ED",
        rearLetters = "B",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSumbaBaratDaya = new CityData {
        cityName = "Kabupaten Sumba Barat Daya",
        areaName = "NTT (Pulau Sumba)",
        areaCode = "ED",
        rearLetters = "C",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSumbaTengah = new CityData {
        cityName = "Kabupaten Sumba Tengah",
        areaName = "NTT (Pulau Sumba)",
        areaCode = "ED",
        rearLetters = "D",
        isInvertedRearLetters = false,
    };

    ///////////////////////////////////////////////////////////////////////
    // --------------------------------------------------------- KALIMANTAN
    ///////////////////////////////////////////////////////////////////////
    public static CityData kotaBanjarmasin = new CityData {
        cityName = "Kota Banjarmasin",
        areaName = "Kalimantan Selatan",
        areaCode = "DA",
        rearLetters = "ACIJNSTVX",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBanjar = new CityData {
        cityName = "Kabupaten Banjar",
        areaName = "Kalimantan Selatan",
        areaCode = "DA",
        rearLetters = "BOQ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenHuluSungaiSelatan = new CityData {
        cityName = "Kabupaten Hulu Sungai Selatan",
        areaName = "Kalimantan Selatan",
        areaCode = "DA",
        rearLetters = "D",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenHuluSungaiTengah = new CityData {
        cityName = "Kabupaten Hulu Sungai Tengah",
        areaName = "Kalimantan Selatan",
        areaCode = "DA",
        rearLetters = "E",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenHuluSungaiUtara = new CityData {
        cityName = "Kabupaten Hulu Sungai Utara",
        areaName = "Kalimantan Selatan",
        areaCode = "DA",
        rearLetters = "F",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKotaBaru = new CityData {
        cityName = "Kabupaten Kota Baru",
        areaName = "Kalimantan Selatan",
        areaCode = "DA",
        rearLetters = "G",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTabalong = new CityData {
        cityName = "Kabupaten Tabalong",
        areaName = "Kalimantan Selatan",
        areaCode = "DA",
        rearLetters = "HU",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTapin = new CityData {
        cityName = "Kabupaten Tapin",
        areaName = "Kalimantan Selatan",
        areaCode = "DA",
        rearLetters = "K",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTanahLaut = new CityData {
        cityName = "Kabupaten Tanah Laut",
        areaName = "Kalimantan Selatan",
        areaCode = "DA",
        rearLetters = "L",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBaritoKuala = new CityData {
        cityName = "Kabupaten Barito Kuala",
        areaName = "Kalimantan Selatan",
        areaCode = "DA",
        rearLetters = "M",
        isInvertedRearLetters = false,
    };
    public static CityData kotaBanjarbaru = new CityData {
        cityName = "Kota Banjarbaru",
        areaName = "Kalimantan Selatan",
        areaCode = "DA",
        rearLetters = "PRW",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBalangan = new CityData {
        cityName = "Kabupaten Balangan",
        areaName = "Kalimantan Selatan",
        areaCode = "DA",
        rearLetters = "Y",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTanahBumbu = new CityData {
        cityName = "Kabupaten Tanah Bumbu",
        areaName = "Kalimantan Selatan",
        areaCode = "DA",
        rearLetters = "Z",
        isInvertedRearLetters = false,
    };
    public static CityData kotaPontianak = new CityData {
        cityName = "Kota Pontianak",
        areaName = "Kalimantan Barat",
        areaCode = "KB",
        rearLetters = "AHNOQSW",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMempawah = new CityData {
        cityName = "Kabupaten Mempawah",
        areaName = "Kalimantan Barat",
        areaCode = "KB",
        rearLetters = "B",
        isInvertedRearLetters = false,
    };
    public static CityData kotaSingkawang = new CityData {
        cityName = "Kota Singkawang",
        areaName = "Kalimantan Barat",
        areaCode = "KB",
        rearLetters = "CY",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSanggau = new CityData {
        cityName = "Kabupaten Sanggau",
        areaName = "Kalimantan Barat",
        areaCode = "KB",
        rearLetters = "DU",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSintang = new CityData {
        cityName = "Kabupaten Sintang",
        areaName = "Kalimantan Barat",
        areaCode = "KB",
        rearLetters = "E",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKapuasHulu = new CityData {
        cityName = "Kabupaten Kapuas Hulu",
        areaName = "Kalimantan Barat",
        areaCode = "KB",
        rearLetters = "F",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKetapang = new CityData {
        cityName = "Kabupaten Ketapang",
        areaName = "Kalimantan Barat",
        areaCode = "KB",
        rearLetters = "G",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMelawi = new CityData {
        cityName = "Kabupaten Melawi",
        areaName = "Kalimantan Barat",
        areaCode = "KB",
        rearLetters = "J",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBengkayang = new CityData {
        cityName = "Kabupaten Bengkayang",
        areaName = "Kalimantan Barat",
        areaCode = "KB",
        rearLetters = "K",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLandak = new CityData {
        cityName = "Kabupaten Landak",
        areaName = "Kalimantan Barat",
        areaCode = "KB",
        rearLetters = "L",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKubuRaya = new CityData {
        cityName = "Kabupaten Kubu Raya",
        areaName = "Kalimantan Barat",
        areaCode = "KB",
        rearLetters = "M",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSambas = new CityData {
        cityName = "Kabupaten Sambas",
        areaName = "Kalimantan Barat",
        areaCode = "KB",
        rearLetters = "PT",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSekadau = new CityData {
        cityName = "Kabupaten Sekadau",
        areaName = "Kalimantan Barat",
        areaCode = "KB",
        rearLetters = "V",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKayonUtara = new CityData {
        cityName = "Kabupaten Kayon Utara",
        areaName = "Kalimantan Barat",
        areaCode = "KB",
        rearLetters = "Z",
        isInvertedRearLetters = false,
    };
    public static CityData kotaPalangkaraya = new CityData {
        cityName = "Kota Palangkaraya",
        areaName = "Kalimantan Tengah",
        areaCode = "KH",
        rearLetters = "ATY",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKapuas = new CityData {
        cityName = "Kabupaten Kapuas",
        areaName = "Kalimantan Tengah",
        areaCode = "KH",
        rearLetters = "BCU",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBaritoSelatan = new CityData {
        cityName = "KabupatenBaritoSelatan",
        areaName = "Kalimantan Tengah",
        areaCode = "KH",
        rearLetters = "D",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBaritoUtara = new CityData {
        cityName = "Kabupaten Barito Utara",
        areaName = "Kalimantan Tengah",
        areaCode = "KH",
        rearLetters = "E",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKotawaringinTimur = new CityData {
        cityName = "Kabupaten Kotawaringin Timur",
        areaName = "Kalimantan Tengah",
        areaCode = "KH",
        rearLetters = "FLW",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKotawaringinBarat = new CityData {
        cityName = "Kabupaten Kotawaringin Barat",
        areaName = "Kalimantan Tengah",
        areaCode = "KH",
        rearLetters = "GV",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenGunungMas = new CityData {
        cityName = "Kabupaten Gunung Mas",
        areaName = "Kalimantan Tengah",
        areaCode = "KH",
        rearLetters = "H",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPulangPisau = new CityData {
        cityName = "Kabupaten Pulang Pisau",
        areaName = "Kalimantan Tengah",
        areaCode = "KH",
        rearLetters = "J",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBaritoTimur = new CityData {
        cityName = "Kabupaten Barito Timur",
        areaName = "Kalimantan Tengah",
        areaCode = "KH",
        rearLetters = "K",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMurungRaya = new CityData {
        cityName = "Kabupaten Murung Raya",
        areaName = "Kalimantan Tengah",
        areaCode = "KH",
        rearLetters = "M",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKatingan = new CityData {
        cityName = "Kabupaten Katingan",
        areaName = "Kalimantan Tengah",
        areaCode = "KH",
        rearLetters = "N",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSeruyan = new CityData {
        cityName = "Kabupaten Seruyan",
        areaName = "Kalimantan Tengah",
        areaCode = "KH",
        rearLetters = "P",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLamandau = new CityData {
        cityName = "Kabupaten Lamandau",
        areaName = "Kalimantan Tengah",
        areaCode = "KH",
        rearLetters = "R",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSukamara = new CityData {
        cityName = "Kabupaten Sukamara",
        areaName = "Kalimantan Tengah",
        areaCode = "KH",
        rearLetters = "S",
        isInvertedRearLetters = false,
    };
    public static CityData kotaBalikpapan = new CityData {
        cityName = "Kota Balikpapan",
        areaName = "Kalimantan Timur",
        areaCode = "KT",
        rearLetters = "AHKLYZ",
        isInvertedRearLetters = false,
    };
    public static CityData kotaSamarinda = new CityData {
        cityName = "Kota Samarinda",
        areaName = "Kalimantan Timur",
        areaCode = "KT",
        rearLetters = "BFIMNSW",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKutaiKartanegara = new CityData {
        cityName = "Kabupaten Kutai Kartanegara",
        areaName = "Kalimantan Timur",
        areaCode = "KT",
        rearLetters = "CJOSU",
        isInvertedRearLetters = false,
    };
    public static CityData kotaBontang = new CityData {
        cityName = "Kota Bontang",
        areaName = "Kalimantan Timur",
        areaCode = "KT",
        rearLetters = "DQ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPaser = new CityData {
        cityName = "Kabupaten Paser",
        areaName = "Kalimantan Timur",
        areaCode = "KT",
        rearLetters = "EJS",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBerau = new CityData {
        cityName = "Kabupaten Berau",
        areaName = "Kalimantan Timur",
        areaCode = "KT",
        rearLetters = "GJS",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKutaiTimur = new CityData {
        cityName = "Kabupaten Kutai Timur",
        areaName = "Kalimantan Timur",
        areaCode = "KT",
        rearLetters = "JRS",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKutaiBarat = new CityData {
        cityName = "Kabupaten Kutai Barat",
        areaName = "Kalimantan Timur",
        areaCode = "KT",
        rearLetters = "P",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMahakamUlu = new CityData {
        cityName = "Kabupaten Mahakam Ulu",
        areaName = "Kalimantan Timur",
        areaCode = "KT",
        rearLetters = "T",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPenajamPaserUtara = new CityData {
        cityName = "Kabupaten Penajam Paser Utara",
        areaName = "Kalimantan Timur",
        areaCode = "KT",
        rearLetters = "V",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBulungan = new CityData {
        cityName = "Kabupaten Bulungan",
        areaName = "Kalimantan Utara",
        areaCode = "KU",
        rearLetters = "AB",
        isInvertedRearLetters = false,
    };
    public static CityData kotaTarakan = new CityData {
        cityName = "Kota Tarakan",
        areaName = "Kalimantan Utara",
        areaCode = "KU",
        rearLetters = "G",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTanaTidung = new CityData {
        cityName = "Kabupaten Tana Tidung",
        areaName = "Kalimantan Utara",
        areaCode = "KU",
        rearLetters = "H",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenNunukan = new CityData {
        cityName = "Kabupaten Nunukan",
        areaName = "Kalimantan Utara",
        areaCode = "KU",
        rearLetters = "N",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMalinau = new CityData {
        cityName = "Kabupaten Malinau",
        areaName = "Kalimantan Utara",
        areaCode = "KU",
        rearLetters = "S",
        isInvertedRearLetters = false,
    };

    ///////////////////////////////////////////////////////////////////////
    // --------------------------------------------------------- GORONTALO SULAWESI
    ///////////////////////////////////////////////////////////////////////
    public static CityData kotaManado = new CityData {
        cityName = "Kota Manado",
        areaName = "Sulawesi Utara",
        areaCode = "DB",
        rearLetters = "ALMR",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMinahasa = new CityData {
        cityName = "Kabupaten Minahasa",
        areaName = "Sulawesi Utara",
        areaCode = "DB",
        rearLetters = "BQ",
        isInvertedRearLetters = false,
    };
    public static CityData kotaBitung = new CityData {
        cityName = "Kota Bitung",
        areaName = "Sulawesi Utara",
        areaCode = "DB",
        rearLetters = "C",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBolaangMongodow = new CityData {
        cityName = "Kabupaten Bolaang Mongodow",
        areaName = "Sulawesi Utara",
        areaCode = "DB",
        rearLetters = "D",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMinahasaSelatan = new CityData {
        cityName = "Kabupaten Minahasa Selatan",
        areaName = "Sulawesi Utara",
        areaCode = "DB",
        rearLetters = "E",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMinahasaUtara = new CityData {
        cityName = "Kabupaten Minahasa Utara",
        areaName = "Sulawesi Utara",
        areaCode = "DB",
        rearLetters = "F",
        isInvertedRearLetters = false,
    };
    public static CityData kotaTomohon = new CityData {
        cityName = "Kota Tomohon",
        areaName = "Sulawesi Utara",
        areaCode = "DB",
        rearLetters = "G",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBolaangMongondowUtara = new CityData {
        cityName = "Kabupaten Bolaang Mongondow Utara",
        areaName = "Sulawesi Utara",
        areaCode = "DB",
        rearLetters = "H",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMinahasaTenggara = new CityData {
        cityName = "Kabupaten Minahasa Tenggara",
        areaName = "Sulawesi Utara",
        areaCode = "DB",
        rearLetters = "J",
        isInvertedRearLetters = false,
    };
    public static CityData kotaKotamubagu = new CityData {
        cityName = "Kota Kotamubagu",
        areaName = "Sulawesi Utara",
        areaCode = "DB",
        rearLetters = "K",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBolaangMongondowTimur = new CityData {
        cityName = "Kabupaten Bolaang Mongodow Timur",
        areaName = "Sulawesi Utara",
        areaCode = "DB",
        rearLetters = "N",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBolaangMongondowSelatan = new CityData {
        cityName = "Kabupaten Bolaang Mongondow Selatan",
        areaName = "Sulawesi Utara",
        areaCode = "DB",
        rearLetters = "P",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMamuju = new CityData {
        cityName = "Kabupaten Mamuju",
        areaName = "Sulawesi Barat",
        areaCode = "DC",
        rearLetters = "AGLP",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMajene = new CityData {
        cityName = "Kabupaten Majene",
        areaName = "Sulawesi Barat",
        areaCode = "DC",
        rearLetters = "BQ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPolewaliMandar = new CityData {
        cityName = "Kabupaten Polewali Mandar",
        areaName = "Sulawesi Barat",
        areaCode = "DC",
        rearLetters = "CN",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMamasa = new CityData {
        cityName = "Kabupaten Mamasa",
        areaName = "Sulawesi Barat",
        areaCode = "DC",
        rearLetters = "D",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPasangkayu = new CityData {
        cityName = "Kabupaten Pasangkayu",
        areaName = "Sulawesi Barat",
        areaCode = "DC",
        rearLetters = "EX",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMamujuTengah = new CityData {
        cityName = "Kabupaten Mamuju Tengah",
        areaName = "Sulawesi Barat",
        areaCode = "DC",
        rearLetters = "F",
        isInvertedRearLetters = false,
    };
    public static CityData kotaMakassar = new CityData {
        cityName = "Kota Makassar",
        areaName = "Sulawesi Selatan (Selatan)",
        areaCode = "DD",
        rearLetters = "AIKMOQRSUVX",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenGowa = new CityData {
        cityName = "Kabupaten Gowa",
        areaName = "Sulawesi Selatan (Selatan)",
        areaCode = "DD",
        rearLetters = "BLNY",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTakalar = new CityData {
        cityName = "Kabupaten Takalar",
        areaName = "Sulawesi Selatan (Selatan)",
        areaCode = "DD",
        rearLetters = "CP",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMaros = new CityData {
        cityName = "Kabupaten Maros",
        areaName = "Sulawesi Selatan (Selatan)",
        areaCode = "DD",
        rearLetters = "DT",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPangkajene = new CityData {
        cityName = "Kabupaten Pangkajene",
        areaName = "Sulawesi Selatan (Selatan)",
        areaCode = "DD",
        rearLetters = "EW",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBantaeng = new CityData {
        cityName = "Kabupaten Bantaeng",
        areaName = "Sulawesi Selatan (Selatan)",
        areaCode = "DD",
        rearLetters = "F",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenJeneponto = new CityData {
        cityName = "Kabupaten Jeneponto",
        areaName = "Sulawesi Selatan (Selatan)",
        areaCode = "DD",
        rearLetters = "G",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBulukumba = new CityData {
        cityName = "Kabupaten Bulukumba",
        areaName = "Sulawesi Selatan (Selatan)",
        areaCode = "DD",
        rearLetters = "HZ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSelayar = new CityData {
        cityName = "Kabupaten Selayar",
        areaName = "Sulawesi Selatan (Selatan)",
        areaCode = "DD",
        rearLetters = "J",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKepulauanSangihe = new CityData {
        cityName = "Kabupaten Kepulauan Sangihe",
        areaName = "Sulawesi Utara (Kepulauan)",
        areaCode = "DL",
        rearLetters = "A",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKepulauanTalaud = new CityData {
        cityName = "Kabupaten Kepulauan Talaud",
        areaName = "Sulawesi Utara (Kepulauan)",
        areaCode = "DL",
        rearLetters = "B",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKepulauanSitaro = new CityData {
        cityName = "Kabupaten Kepulauan Sitaro",
        areaName = "Sulawesi Utara (Kepulauan)",
        areaCode = "DL",
        rearLetters = "C",
        isInvertedRearLetters = false,
    };
    public static CityData kotaGorontalo = new CityData {
        cityName = "Kota Gorontalo",
        areaName = "Gorontalo",
        areaCode = "DM",
        rearLetters = "AJX",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenGorontalo = new CityData {
        cityName = "Kabupaten Gorontalo",
        areaName = "Gorontalo",
        areaCode = "DM",
        rearLetters = "BH",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBoalemo = new CityData {
        cityName = "Kabupaten Boalemo",
        areaName = "Gorontalo",
        areaCode = "DM",
        rearLetters = "C",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPohuwato = new CityData {
        cityName = "Kabupaten Pohuwato",
        areaName = "Gorontalo",
        areaCode = "DM",
        rearLetters = "D",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBoneBolango = new CityData {
        cityName = "Kabupaten Bone Bolango",
        areaName = "Gorontalo",
        areaCode = "DM",
        rearLetters = "E",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenGorontaloUtara = new CityData {
        cityName = "Kabupaten Gorontalo Utara",
        areaName = "Gorontalo",
        areaCode = "DM",
        rearLetters = "F",
        isInvertedRearLetters = false,
    };
    public static CityData kotaPalu = new CityData {
        cityName = "Kota Palu",
        areaName = "Sulawesi Tengah",
        areaCode = "DN",
        rearLetters = "AINVY",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenDonggala = new CityData {
        cityName = "Kabupaten Donggala",
        areaName = "Sulawesi Tengah",
        areaCode = "DN",
        rearLetters = "BJ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBanggai = new CityData {
        cityName = "Kabupaten Banggai",
        areaName = "Sulawesi Tengah",
        areaCode = "DN",
        rearLetters = "CR",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenToliToli = new CityData {
        cityName = "Kabupaten Toli-Toli",
        areaName = "Sulawesi Tengah",
        areaCode = "DN",
        rearLetters = "D",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPoso = new CityData {
        cityName = "Kabupaten Poso",
        areaName = "Sulawesi Tengah",
        areaCode = "DN",
        rearLetters = "E",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBuol = new CityData {
        cityName = "Kabupaten Buol",
        areaName = "Sulawesi Tengah",
        areaCode = "DN",
        rearLetters = "F",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMorowali = new CityData {
        cityName = "Kabupaten Morowali",
        areaName = "Sulawesi Tengah",
        areaCode = "DN",
        rearLetters = "G",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBanggaiKepulauan = new CityData {
        cityName = "Kabupaten Banggai Kepulauan",
        areaName = "Sulawesi Tengah",
        areaCode = "DN",
        rearLetters = "H",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenParigiMoutong = new CityData {
        cityName = "Kabupaten Parigin Moutong",
        areaName = "Sulawesi Tengah",
        areaCode = "DN",
        rearLetters = "KP",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTojoUnaUna = new CityData {
        cityName = "Kabupaten TOjo Una-Una",
        areaName = "Sulawesi Tengah",
        areaCode = "DN",
        rearLetters = "L",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSigi = new CityData {
        cityName = "Kabupaten Sigi",
        areaName = "Sulawesi Tengah",
        areaCode = "DN",
        rearLetters = "M",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBanggaiLaut = new CityData {
        cityName = "Kabupaten Banggai Laut",
        areaName = "Sulawesi Tengah",
        areaCode = "DN",
        rearLetters = "Q",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMorowaliUtara = new CityData {
        cityName = "Kabupaten Morowali Utara",
        areaName = "Sulawesi Tengah",
        areaCode = "DN",
        rearLetters = "U",
        isInvertedRearLetters = false,
    };
    public static CityData kotaParepare = new CityData {
        cityName = "Kota Parepare",
        areaName = "Sulawesi Selatan (Utara)",
        areaCode = "DP",
        rearLetters = "ALM",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBarru = new CityData {
        cityName = "KabupatenBarru",
        areaName = "Sulawesi Selatan (Utara)",
        areaCode = "DP",
        rearLetters = "B",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSidenrengRappang = new CityData {
        cityName = "Kabupaten Sidenreng Rappang",
        areaName = "Sulawesi Selatan (Utara)",
        areaCode = "DP",
        rearLetters = "CPQ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPinrang = new CityData {
        cityName = "Kabupaten Pinrang",
        areaName = "Sulawesi Selatan (Utara)",
        areaCode = "DP",
        rearLetters = "DRS",
        isInvertedRearLetters = false,
    };
    public static CityData kotaPalopo = new CityData {
        cityName = "Kota Palopo",
        areaName = "Sulawesi Selatan (Utara)",
        areaCode = "DP",
        rearLetters = "ET",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLuwu = new CityData {
        cityName = "Kabupaten Luwu",
        areaName = "Sulawesi Selatan (Utara)",
        areaCode = "DP",
        rearLetters = "FU",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLuwuTimur = new CityData {
        cityName = "Kabupaten Luwu Timur",
        areaName = "Sulawesi Selatan (Utara)",
        areaCode = "DP",
        rearLetters = "GV",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenLuwuUtara = new CityData {
        cityName = "Kabupaten Luwu Utara",
        areaName = "Sulawesi Selatan (Utara)",
        areaCode = "DP",
        rearLetters = "H",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenEnrekang = new CityData {
        cityName = "Kabupaten Enrekang",
        areaName = "Sulawesi Selatan (Utara)",
        areaCode = "DP",
        rearLetters = "IX",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTanaToraja = new CityData {
        cityName = "Kabupaten Tana Toraja",
        areaName = "Sulawesi Selatan (Utara)",
        areaCode = "DP",
        rearLetters = "JY",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTorajaUtara = new CityData {
        cityName = "Kabupaten Toraja Utara",
        areaName = "Sulawesi Selatan (Utara)",
        areaCode = "DP",
        rearLetters = "KZ",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKonawe = new CityData {
        cityName = "Kabupaten Konawe",
        areaName = "Sulawesi Tenggara",
        areaCode = "DT",
        rearLetters = "A",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKolaka = new CityData {
        cityName = "Kabupaten Kolaka",
        areaName = "Sulawesi Tenggara",
        areaCode = "DT",
        rearLetters = "B",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenButon = new CityData {
        cityName = "Kabupaten Buton",
        areaName = "Sulawesi Tenggara",
        areaCode = "DT",
        rearLetters = "C",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMuna = new CityData {
        cityName = "Kabupaten Muna",
        areaName = "Sulawesi Tenggara",
        areaCode = "DT",
        rearLetters = "D",
        isInvertedRearLetters = false,
    };
    public static CityData kotaKendari = new CityData {
        cityName = "Kota Kendari",
        areaName = "Sulawesi Tenggara",
        areaCode = "DT",
        rearLetters = "EF",
        isInvertedRearLetters = false,
    };
    public static CityData kotaBaubau = new CityData {
        cityName = "Kota Baubau",
        areaName = "Sulawesi Tenggara",
        areaCode = "DT",
        rearLetters = "G",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKonaweSelatan = new CityData {
        cityName = "Kabupaten Konawe Selatan",
        areaName = "Sulawesi Tenggara",
        areaCode = "DT",
        rearLetters = "H",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKolakaUtara = new CityData {
        cityName = "Kabupaten Kolaka Utara",
        areaName = "Sulawesi Tenggara",
        areaCode = "DT",
        rearLetters = "J",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBombana = new CityData {
        cityName = "Kabupaten Bombana",
        areaName = "Sulawesi Tenggara",
        areaCode = "DT",
        rearLetters = "K",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenWakatobi = new CityData {
        cityName = "Kabupaten Wakatobi",
        areaName = "Sulawesi Tenggara",
        areaCode = "DT",
        rearLetters = "L",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKonaweUtara = new CityData {
        cityName = "Kabupaten Konawe Utara",
        areaName = "Sulawesi Tenggara",
        areaCode = "DT",
        rearLetters = "M",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenButonUtara = new CityData {
        cityName = "Kabupaten Buton Utara",
        areaName = "Sulawesi Tenggara",
        areaCode = "DT",
        rearLetters = "N",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKonaweKepulauan = new CityData {
        cityName = "Kabupaten Konawe Kepulauan",
        areaName = "Sulawesi Tenggara",
        areaCode = "DT",
        rearLetters = "O",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMunaBarat = new CityData {
        cityName = "Kabupaten Muna Barat",
        areaName = "Sulawesi Tenggara",
        areaCode = "DT",
        rearLetters = "R",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKolakaTimur = new CityData {
        cityName = "Kabupaten Kolaka Timur",
        areaName = "Sulawesi Tenggara",
        areaCode = "DT",
        rearLetters = "T",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenButonSelatan = new CityData {
        cityName = "Kabupaten Buton Selatan",
        areaName = "Sulawesi Tenggara",
        areaCode = "DT",
        rearLetters = "W",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenButonTengah = new CityData {
        cityName = "Kabupaten Buton Tengah",
        areaName = "Sulawesi Tenggara",
        areaCode = "DT",
        rearLetters = "Y",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBone = new CityData {
        cityName = "Kabupaten Bone",
        areaName = "Sulawesi Selatan (Tengah)",
        areaCode = "DW",
        rearLetters = "AEFGH",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenWajo = new CityData {
        cityName = "Kabupaten Wajo",
        areaName = "Sulawesi Selatan (Tengah)",
        areaCode = "DW",
        rearLetters = "BLMNOP",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSoppeng = new CityData {
        cityName = "Kabupaten Soppeng",
        areaName = "Sulawesi Selatan (Tengah)",
        areaCode = "DW",
        rearLetters = "CQY",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSinjai = new CityData {
        cityName = "Kabupaten Sinjai",
        areaName = "Sulawesi Selatan (Tengah)",
        areaCode = "DW",
        rearLetters = "DVZ",
        isInvertedRearLetters = false,
    };

    ///////////////////////////////////////////////////////////////////////
    // --------------------------------------------------------- MALUKU PAPUA
    ///////////////////////////////////////////////////////////////////////
    public static CityData kotaAmbon = new CityData
    {
        cityName = "Kota Ambon",
        areaName = "Maluku",
        areaCode = "DE",
        rearLetters = "ALN",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMalukuTengah = new CityData
    {
        cityName = "Kabupaten Maluku Tengah",
        areaName = "Maluku",
        areaCode = "DE",
        rearLetters = "B",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMalukuTenggara = new CityData
    {
        cityName = "Kabupaten Maluku Tenggara",
        areaName = "Maluku",
        areaCode = "DE",
        rearLetters = "C",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBuru = new CityData
    {
        cityName = "Kabupaten Buru",
        areaName = "Maluku",
        areaCode = "DE",
        rearLetters = "D",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKepulauanTanimbar = new CityData
    {
        cityName = "Kabupaten Kepulauan Tanimbar",
        areaName = "Maluku",
        areaCode = "DE",
        rearLetters = "E",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKepulauanAru = new CityData
    {
        cityName = "Kabupaten Kepulauan Aru",
        areaName = "Maluku",
        areaCode = "DE",
        rearLetters = "F",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSeramBarat = new CityData
    {
        cityName = "Kabupaten Seram Barat",
        areaName = "Maluku",
        areaCode = "DE",
        rearLetters = "G",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSeramTimur = new CityData
    {
        cityName = "Kabupaten Seram Timur",
        areaName = "Maluku",
        areaCode = "DE",
        rearLetters = "H",
        isInvertedRearLetters = false,
    };
    public static CityData kotaTual = new CityData
    {
        cityName = "Kota Tual",
        areaName = "Maluku",
        areaCode = "DE",
        rearLetters = "I",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMalukuBaratDaya = new CityData
    {
        cityName = "Kabupaten Maluku Barat Daya",
        areaName = "Maluku",
        areaCode = "DE",
        rearLetters = "J",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBuruSelatan = new CityData
    {
        cityName = "Kabupaten Buru Selatan",
        areaName = "Maluku",
        areaCode = "DE",
        rearLetters = "K",
        isInvertedRearLetters = false,
    };
    public static CityData kotaTernate = new CityData
    {
        cityName = "KOta Ternate",
        areaName = "Maluku Utara",
        areaCode = "DG",
        rearLetters = "AKQ",
        isInvertedRearLetters = false,
    };
    public static CityData kotaTidore = new CityData
    {
        cityName = "Kota Tidore",
        areaName = "Maluku Utara",
        areaCode = "DG",
        rearLetters = "BL",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTaliabu = new CityData
    {
        cityName = "Kabupaten Taliabu",
        areaName = "Maluku Utara",
        areaCode = "DG",
        rearLetters = "H",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMorotai = new CityData
    {
        cityName = "Kabupaten Morotai",
        areaName = "Maluku Utara",
        areaCode = "DG",
        rearLetters = "J",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenHalmaheraBarat = new CityData
    {
        cityName = "Kabupaten Halmahera Barat",
        areaName = "Maluku Utara",
        areaCode = "DG",
        rearLetters = "M",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenHalmaheraUtara = new CityData
    {
        cityName = "Kabupaten Halmahera Utara",
        areaName = "Maluku Utara",
        areaCode = "DG",
        rearLetters = "N",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenHalmaheraSelatan = new CityData
    {
        cityName = "Kabupaten Halmahera Selatan",
        areaName = "Maluku Utara",
        areaCode = "DG",
        rearLetters = "P",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSula = new CityData
    {
        cityName = "Kabupaten Sula",
        areaName = "Maluku Utara",
        areaCode = "DG",
        rearLetters = "R",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenHalmaheraTengah = new CityData
    {
        cityName = "Kabupaten Halmahera Tengah",
        areaName = "Maluku Utara",
        areaCode = "DG",
        rearLetters = "S",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenHalmaheraTimur = new CityData
    {
        cityName = "Kabupaten Halmahera Timur",
        areaName = "Maluku Utara",
        areaCode = "DG",
        rearLetters = "T",
        isInvertedRearLetters = false,
    };
    public static CityData kotaJayapura = new CityData
    {
        cityName = "Kota Jayapura",
        areaName = "Papua Selatan, Tengah & Pegunungan",
        areaCode = "PA",
        rearLetters = "AFR",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenJayawijaya = new CityData
    {
        cityName = "Kabupaten Jayawijaya",
        areaName = "Papua Selatan, Tengah & Pegunungan",
        areaCode = "PA",
        rearLetters = "BY",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBiakNumfor = new CityData
    {
        cityName = "Kabupaten Biak Numfor",
        areaName = "Papua Selatan, Tengah & Pegunungan",
        areaCode = "PA",
        rearLetters = "C",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMimika = new CityData
    {
        cityName = "Kabupaten Mimika",
        areaName = "Papua Selatan, Tengah & Pegunungan",
        areaCode = "PA",
        rearLetters = "DHM",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPaniai = new CityData
    {
        cityName = "Kabupaten Paniai",
        areaName = "Papua Selatan, Tengah & Pegunungan",
        areaCode = "PA",
        rearLetters = "E",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMerauke = new CityData
    {
        cityName = "Kabupaten Merauke",
        areaName = "Papua Selatan, Tengah & Pegunungan",
        areaCode = "PA",
        rearLetters = "G",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenJayapura = new CityData
    {
        cityName = "Kabupaten Jayapura",
        areaName = "Papua Selatan, Tengah & Pegunungan",
        areaCode = "PA",
        rearLetters = "J",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenNabire = new CityData
    {
        cityName = "Kabupaten Nabire",
        areaName = "Papua Selatan, Tengah & Pegunungan",
        areaCode = "PA",
        rearLetters = "K",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenYapen = new CityData
    {
        cityName = "Kabupaten Yapen",
        areaName = "Papua Selatan, Tengah & Pegunungan",
        areaCode = "PA",
        rearLetters = "L",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenWaropen = new CityData
    {
        cityName = "Kabupaten Waropen",
        areaName = "Papua Selatan, Tengah & Pegunungan",
        areaCode = "PA",
        rearLetters = "N",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPuncakJaya = new CityData
    {
        cityName = "Kabupaten Puncak Jaya",
        areaName = "Papua Selatan, Tengah & Pegunungan",
        areaCode = "PA",
        rearLetters = "P",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKeerom = new CityData
    {
        cityName = "Kabupaten Keerom",
        areaName = "Papua Selatan, Tengah & Pegunungan",
        areaCode = "PA",
        rearLetters = "Q",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSarmi = new CityData
    {
        cityName = "Kabupaten Sarmi",
        areaName = "Papua Selatan, Tengah & Pegunungan",
        areaCode = "PA",
        rearLetters = "S",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMappi = new CityData
    {
        cityName = "Kabupaten Mappi",
        areaName = "Papua Selatan, Tengah & Pegunungan",
        areaCode = "PA",
        rearLetters = "T",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSupiori = new CityData
    {
        cityName = "Kabupaten Supiori",
        areaName = "Papua Selatan, Tengah & Pegunungan",
        areaCode = "PA",
        rearLetters = "U",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenBovenGigoel = new CityData
    {
        cityName = "Kabupaten Boven Digoel",
        areaName = "Papua Selatan, Tengah & Pegunungan",
        areaCode = "PA",
        rearLetters = "V",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMamberanoRaya = new CityData
    {
        cityName = "Kabupaten Mamberano Raya",
        areaName = "Papua Selatan, Tengah & Pegunungan",
        areaCode = "PA",
        rearLetters = "X",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTolikara = new CityData
    {
        cityName = "Kabupaten Tolikara",
        areaName = "Papua Selatan, Tengah & Pegunungan",
        areaCode = "PA",
        rearLetters = "Z",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSorong = new CityData
    {
        cityName = "Kabupaten Sorong",
        areaName = "Papua Barat & Barat Daya",
        areaCode = "PB",
        rearLetters = "A",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTelukBintuni = new CityData
    {
        cityName = "Kabupaten Teluk Bintuni",
        areaName = "Papua Barat & Barat Daya",
        areaCode = "PB",
        rearLetters = "B",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenPegununganArfak = new CityData
    {
        cityName = "Kabupaten Pegunungan Arfak",
        areaName = "Papua Barat & Barat Daya",
        areaCode = "PB",
        rearLetters = "D",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenFakfak = new CityData
    {
        cityName = "Kabupaten Fakfak",
        areaName = "Papua Barat & Barat Daya",
        areaCode = "PB",
        rearLetters = "F",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenManokwari = new CityData
    {
        cityName = "Kabupaten Manokwari",
        areaName = "Papua Barat & Barat Daya",
        areaCode = "PB",
        rearLetters = "GHM",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenKaimana = new CityData
    {
        cityName = "Kabupaten Kaimana",
        areaName = "Papua Barat & Barat Daya",
        areaCode = "PB",
        rearLetters = "K",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenManokwariSelatan = new CityData
    {
        cityName = "Kabupaten Manokwari Selatan",
        areaName = "Papua Barat & Barat Daya",
        areaCode = "PB",
        rearLetters = "L",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTambrauw = new CityData
    {
        cityName = "Kabupaten Tambrauw",
        areaName = "Papua Barat & Barat Daya",
        areaCode = "PB",
        rearLetters = "P",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenRajaAmpat = new CityData
    {
        cityName = "Kabupaten Raja Ampat",
        areaName = "Papua Barat & Barat Daya",
        areaCode = "PB",
        rearLetters = "R",
        isInvertedRearLetters = false,
    };
    public static CityData kotaSorong = new CityData
    {
        cityName = "Kota Sorong",
        areaName = "Papua Barat & Barat Daya",
        areaCode = "PB",
        rearLetters = "S",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenSorongSelatan = new CityData
    {
        cityName = "Kabupaten Sorong Selatan",
        areaName = "Papua Barat & Barat Daya",
        areaCode = "PB",
        rearLetters = "T",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenMaybrat = new CityData
    {
        cityName = "Kabupaten Maybrat",
        areaName = "Papua Barat & Barat Daya",
        areaCode = "PB",
        rearLetters = "V",
        isInvertedRearLetters = false,
    };
    public static CityData kabupatenTelukWondama = new CityData
    {
        cityName = "Kabupaten Teluk Wondama",
        areaName = "Papua Barat & Barat Daya",
        areaCode = "PB",
        rearLetters = "W",
        isInvertedRearLetters = false,
    };
}

// Initialization template 
/*
    public static CityData TEMPLATE = new CityData {
        cityName = "TEMPLATE",
        areaName = "TEMPLATE",
        areaCode = "TEMPLATE",
        rearLetters = "TEMPLATE",
        isInvertedRearLetters = false,
    };
*/